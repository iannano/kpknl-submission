<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Landing Routes
Route::get('/', function () {
    return view('landing');
});

//Frontend Routes
Route::group(['namespace'=>'Frontend'], function(){

    //Auth routes
    Route::group(['namespace'=>'Auth','prefix'=>'/auth'], function(){

        //Show /auth/login
        Route::get('/login',[
            "as"    => "login",
            "uses"  => "Login@view"
        ]);

        //Proccess form at /auth/login
        Route::post('/login/submit',[
            "as"    => "login.submit",
            "uses"  => "Login@submit"
        ])->middleware('auth.satuan-kerja');

        //Proccess logout
        Route::get('/logout',[
            "as"    => "logout",
            "uses"  => "Logout@view"
        ]);

        //Show /auth/forget-password
        Route::get('/forget-password',[
            "as"    => "forget-password",
            "uses"  => "ForgetPassword@view"
        ]);

        //Submit form in /auth/forget-password
        //TODO: Change to POST Method
        Route::get('/forget-password/submit',[
            "as"    => "forget-password.submit",
            "uses"  => "ForgetPassword@submit"
        ]);
    });

    //Peraturan routes
    Route::group(['namespace'=>'Peraturan', 'prefix'=>'peraturan'],function() {

        //Show /peraturan/
        Route::get('/', [
            "as" => "peraturan",
            "uses" => "Index@view"
        ]);
    });

    //Pelayanan routes
    Route::group(['namespace'=>'Pelayanan', 'prefix'=>'pelayanan'], function(){

        //Show /pelayanan
        Route::get('/',[
            "as"    => "pelayanan",
            "uses"  => "Index@view"
        ]);

        //Show /pelayanan/{slugseksi}
        Route::get('/{slugseksi?}',[
            "as"    => "pelayanan.detail",
            "uses"  => "Detail@view"
        ]);

        //Show /pelayanan/{slugseksi}/{slugpelayanan}
        Route::get('/{slugseksi?}/{slugpelayanan?}',[
            "as"    => "pelayanan.prosedur",
            "uses"  => "Prosedur@view"
        ]);

        Route::post('/{slugseksi?}/{slugpelayanan?}/submit',[
            "as"    => "pelayanan.prosedur.submit",
            "uses"  => "Prosedur@submit"
        ]);
    });

    //Dashboard routes
    Route::group(['namespace'=>'Dashboard','prefix'=>'dashboard'], function(){
        Route::get('/',[
            "as"    => "dashboard.index",
            "uses"  => "Index@view"
        ]);
    });

    //Submission routes
    Route::group(['namespace'=>'PengajuanPelayanan','prefix'=>'pengajuan-pelayanan'], function(){

        //Index
        Route::get('/', [
            "as"    => "pengajuan-pelayanan.index",
            "uses"  => "Index@view"
        ]);

        Route::get('/pelayanan',[
            "as"    => "pengajuan-pelayanan.pelayanan",
            "uses"  => "Pelayanan@view"
        ]);

        //Process pengajuan pelayanan
        Route::post('/pelayanan/submit',[
            "as"    => "pengajuan-pelayanan.pelayanan.submit",
            "uses"  => "Pelayanan@submit"
        ]);

        Route::get('/dokumen',[
            "as"    => "pengajuan-pelayanan.dokumen",
            "uses"  => "Dokumen@view"
        ]);

        Route::get('/dokumen/submit',[
            "as"    => "pengajuan-pelayanan.dokumen.submit",
            "uses"  => "Dokumen@submit"
        ]);

        Route::get('/tiket',[
            "as"    => "pengajuan-pelayanan.tiket",
            "uses"  => "Tiket@view"
        ]);
    });

    //Pelacakan routes
    Route::group(['namespace'=>'Pelacakan','prefix'=>'pelacakan'], function(){
        Route::get('/',[
            "as"    => "pelacakan.index",
            "uses"  => "Index@view"
        ]);

        Route::post('/hasil-pelacakan',[
            "as"    => "pelacakan.index.submit",
            "uses"  => "Index@submit"
        ]);
    });
});

//Backend Routes
Route::group(['namespace' => 'Backend','prefix'=>'internal-kpknl'], function(){

    //Dashboard
    Route::get('/dashboard',[
        "as"    => "internal-kpknl.dashboard",
        "uses"  => "Dashboard@view"
    ]);

    //Auth
    Route::group(['namespace'=>'Auth','prefix'=>'auth'], function (){

        //Login
        Route::get('/login',[
            "as"    => "internal-kpknl.auth.login",
            "uses"  => "Login@view"
        ]);

        Route::post('/login/submit',[
            "as"    => "internal-kpknl.auth.login.submit",
            "uses"  => "Login@submit"
        ])->middleware("auth.internal-kpknl");

        //Logout
        Route::get('/logout',[
            "as"    => "internal-kpknl.auth.logout",
            "uses"  => "Logout@view"
        ]);
    });

    //Administrator
    Route::group(['namespace'=>'Administrator','prefix'=>'administrator'], function(){

        //Manajemen pelayanan
        Route::group(['namespace'=>'ManajemenPelayanan','prefix'=>'manajemen-pelayanan'], function(){

            //List pelayanan
            Route::get('/', [
                "as"    => "internal-kpknl.manajemen-pelayanan.index",
                "uses"  => "Index@view"
            ]);

            //Form create pelayanan
            Route::get('/create', [
                "as"    => "internal-kpknl.manajemen-pelayanan.create",
                "uses"  => "Create@view"
            ]);

            //Proccess data from form create pelayanan
            Route::post('/create/submit', [
                "as"    => "internal-kpknl.manajemen-pelayanan.create.submit",
                "uses"  => "Create@submit"
            ]);

            //Form update pelayanan
            Route::get('/update', [
                "as"    => "internal-kpknl.manajemen-pelayanan.update",
                "uses"  => "Update@view"
            ]);

            //Proccess data from form update pelayanan
            Route::post('/update/submit', [
                "as"    => "internal-kpknl.manajemen-pelayanan.update.submit",
                "uses"  => "Update@submit"
            ]);

            //Form delete pelayanan
            Route::get('/delete', [
                "as"    => "internal-kpknl.manajemen-pelayanan.delete",
                "uses"  => "Delete@view"
            ]);

            //Proccess Data from form delete pelayanan
            Route::post('/delete/submit', [
                "as"    => "internal-kpknl.manajemen-pelayanan.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });

        //Manajemen prosedur
        Route::group(['namespace'=>'ManajemenProsedur','prefix'=>'manajemen-prosedur'], function (){

            //Index manajemen prosedur
            Route::get('/',[
                "as"    => "internal-kpknl.manajemen-prosedur.index",
                "uses"  => "Index@view"
            ]);

            //Create manajemen prosedur
            Route::get('/create',[
                "as"    => "internal-kpknl.manajemen-prosedur.create",
                "uses"  => "Create@view"
            ]);

            //Process data from form create prosedur
            Route::post('/create/submit',[
                "as"    => "internal-kpknl.manajemen-prosedur.create.submit",
                "uses"  => "Create@submit"
            ]);

            //Update manajemen prosedur
            Route::get('/update',[
                "as"    => "internal-kpknl.manajemen-prosedur.update",
                "uses"  => "Update@view"
            ]);

            //Process data from form update prosedur
            Route::post('/update/submit',[
                "as"    => "internal-kpknl.menejemen-prosedur.update.submit",
                "uses"  => "Update@submit"
            ]);

            //Delete manajemen prosedur
            Route::get('/delete',[
                "as"    => "internal-kpknl.manajemen-prosedur.delete",
                "uses"  => "Delete@view"
            ]);

            //Process data from form delete prosedur
            Route::post('/delete/submit',[
                "as"    => "internal-kpknl.manajemen-prosedur.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });

        //Manajemen file pelayanan
        Route::group(['namespace'=>'ManajemenDokumenPelayanan','prefix'=>'manajamen-dokumen'], function (){

            //Index
            Route::get('/',[
                "as"    => "internal-kpknl.manajemen-dokumen.index",
                "uses"  => "Index@view"
            ]);

            //Form create file pelayanan
            Route::get('/create',[
                "as"    => "internal-kpknl.manajemen-dokumen.create",
                "uses"  => "Create@view"
            ]);


            Route::post('/create/submit',[
                "as"    => "internal-kpknl.manajemen-dokumen.create.submit",
                "uses"  => "Create@submit"
            ]);

            //Form update file pelayanan
            Route::get('/update',[
                "as"    => "internal-kpknl.manajemen-dokumen.update",
                "uses"  => "Update@view"
            ]);

            Route::post('/update/submit',[
                "as"    => "internal-kpknl.manajemen-dokumen.update.submit",
                "uses"  => "Update@submit"
            ]);

            //Form delete file pelayanan
            Route::get('/delete',[
                "as"    => "internal-kpknl.manajemen-dokumen.delete",
                "uses"  => "Delete@view"
            ]);

            Route::post('/delete/submit',[
                "as"    => "internal-kpknl.manajemen-dokumen.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });

        //Manajemen pegawai
        Route::group(['namespace'=>'ManajemenPegawai','prefix'=>'manajemen-pegawai'], function (){

            Route::get('/',[
                "as"    => "internal-kpknl.manajemen-pegawai.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/create',[
                "as"    => "internal-kpknl.manajemen-pegawai.create",
                "uses"  => "Create@view"
            ]);

            Route::post('/create/post',[
                "as"    => "internal-kpknl.manajemen-pegawai.create.submit",
                "uses"  => "Create@submit"
            ]);

            Route::get('/update',[
                "as"    => "internal-kpknl.manajemen-pegawai.update",
                "uses"  => "Update@view"
            ]);

            Route::post('/update/submit',[
                "as"    => "internal-kpknl.manajemen-pegawai.update.submit",
                "uses"  => "Update@submit"
            ]);

            Route::get('/delete',[
                "as"    => "internal-kpknl.manajemen-pegawai.delete",
                "uses"  => "Delete@view"
            ]);

            Route::post('/delete/submit',[
                "as"    => "internal-kpknl.manajemen-pegawai.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });

        //Manajemen satuan kerja
        Route::group(['namespace'=>'ManajemenSatuanKerja','prefix'=>'manajemen-satuan-kerja'], function(){

            Route::get('/',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/create',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.create",
                "uses"  => "Create@view"
            ]);

            Route::post('/create/submit',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.create.submit",
                "uses"  => "Create@submit"
            ]);

            Route::get('/update',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.update",
                "uses"  => "Update@view"
            ]);

            Route::post('/update/submit',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.update.submit",
                "uses"  => "Update@submit"
            ]);

            Route::get('/delete',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.delete",
                "uses"  => "Delete@view"
            ]);

            Route::post('/delete/submit',[
                "as"    => "internal-kpknl.manajemen-satuan-kerja.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });

        //Manajemen pic satuan kerja
        Route::group(['namespace'=>'ManajemenPicSatuanKerja','prefix'=>'manajemen-pic-satuan-kerja'], function(){

            Route::get('/', [
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/create',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.create",
                "uses"  => "Create@view"
            ]);

            Route::post('/create/submit',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.create.submit",
                "uses"  => "Create@submit"
            ]);

            Route::get('/update',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.update",
                "uses"  => "Update@view"
            ]);

            Route::post('/update/submit',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.update.submit",
                "uses"  => "Update@submit"
            ]);

            Route::get('/delete',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.delete",
                "uses"  => "Delete@view"
            ]);

            Route::post('/delete/submit',[
                "as"    => "internal-kpknl.manajemen-pic-satuan-kerja.delete.submit",
                "uses"  => "Delete@submit"
            ]);
        });
    });

    //Module Pkn
    Route::group(['namespace'=>'Pkn','prefix'=>'pkn'], function(){

        //Manajemen permohonan masuk
        Route::group(['namespace'=>'PermohonanMasuk','prefix'=>'permohonan-masuk'], function(){

            //List permohonan masuk
            Route::get('/',[
                "as"    => "internal-kpknl.pkn.permohonan-masuk.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/assign-job',[
                "as"    => "internal-kpknl.pkn.permohonan-masuk.assign-job",
                "uses"  => "AssignJob@view"
            ]);

            Route::post('/assign-job/submit',[
                "as"    => "internal-kpknl.pkn.permohonan-masuk.assign-job.submit",
                "uses"  => "AssignJob@submit"
            ]);
        });

        Route::group(['namespace'=>'DalamProses','prefix'=>'dalam-proses'], function(){
            Route::get('/',[
                "as"    => "internal-kpknl.pkn.dalam-proses.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/current-step',[
                "as"    => "internal-kpknl.pkn.dalam-proses.current-step",
                "uses"  => "CurrentStep@view"
            ]);

            Route::post('/current-step/submit',[
                "as"    => "internal-kpknl.pkn.dalam-proses.current-step.submit",
                "uses"  => "CurrentStep@submit"
            ]);

            Route::get('/transfer-job',[
                "as"    => "internal-kpknl.pkn.dalam-proses.transfer-job",
                "uses"  => "TransferJob@view"
            ]);

            Route::post('/transfer-job/submit',[
                "as"    => "internal-kpknl.pkn.dalam-proses.transfer-job.submit",
                "uses"  => "TransferJob@submit"
            ]);

            Route::get('/job-done/submit',[
                "as"    => "internal-kpknl.pkn.dalam-proses.job-done.submit",
                "uses"  => "JobDone@submit"
            ]);
        });
    });

    //Module Penilaian
    Route::group(['namespace'=>'Penilaian','prefix'=>'penilaian'], function(){

        //Manajemen permohonan masuk
        Route::group(['namespace'=>'PermohonanMasuk','prefix'=>'permohonan-masuk'], function(){

            //List permohonan masuk
            Route::get('/',[
                "as"    => "internal-kpknl.penilaian.permohonan-masuk.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/assign-job',[
                "as"    => "internal-kpknl.penilaian.permohonan-masuk.assign-job",
                "uses"  => "AssignJob@view"
            ]);

            Route::post('/assign-job/submit',[
                "as"    => "internal-kpknl.penilaian.permohonan-masuk.assign-job.submit",
                "uses"  => "AssignJob@submit"
            ]);
        });

        Route::group(['namespace'=>'DalamProses','prefix'=>'dalam-proses'], function(){
            Route::get('/',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/current-step',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.current-step",
                "uses"  => "CurrentStep@view"
            ]);

            Route::post('/current-step/submit',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.current-step.submit",
                "uses"  => "CurrentStep@submit"
            ]);

            Route::get('/transfer-job',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.transfer-job",
                "uses"  => "TransferJob@view"
            ]);

            Route::post('/transfer-job/submit',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.transfer-job.submit",
                "uses"  => "TransferJob@submit"
            ]);

            Route::get('/job-done/submit',[
                "as"    => "internal-kpknl.penilaian.dalam-proses.job-done.submit",
                "uses"  => "JobDone@submit"
            ]);
        });
    });

    //Module Lelang
    Route::group(['namespace'=>'Lelang','prefix'=>'lelang'], function(){

        //Manajemen permohonan masuk
        Route::group(['namespace'=>'PermohonanMasuk','prefix'=>'permohonan-masuk'], function(){

            //List permohonan masuk
            Route::get('/',[
                "as"    => "internal-kpknl.lelang.permohonan-masuk.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/assign-job',[
                "as"    => "internal-kpknl.lelang.permohonan-masuk.assign-job",
                "uses"  => "AssignJob@view"
            ]);

            Route::post('/assign-job/submit',[
                "as"    => "internal-kpknl.lelang.permohonan-masuk.assign-job.submit",
                "uses"  => "AssignJob@submit"
            ]);
        });

        Route::group(['namespace'=>'DalamProses','prefix'=>'dalam-proses'], function(){
            Route::get('/',[
                "as"    => "internal-kpknl.lelang.dalam-proses.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/current-step',[
                "as"    => "internal-kpknl.lelang.dalam-proses.current-step",
                "uses"  => "CurrentStep@view"
            ]);

            Route::post('/current-step/submit',[
                "as"    => "internal-kpknl.lelang.dalam-proses.current-step.submit",
                "uses"  => "CurrentStep@submit"
            ]);

            Route::get('/job-done/submit',[
                "as"    => "internal-kpknl.lelang.dalam-proses.job-done.submit",
                "uses"  => "JobDone@submit"
            ]);
        });
    });

    //Module Piutang Negara
    Route::group(['namespace'=>'PiutangNegara','prefix'=>'piutang-negara'], function(){

        //Manajemen permohonan masuk
        Route::group(['namespace'=>'PermohonanMasuk','prefix'=>'permohonan-masuk'], function(){

            //List permohonan masuk
            Route::get('/',[
                "as"    => "internal-kpknl.piutang-negara.permohonan-masuk.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/assign-job',[
                "as"    => "internal-kpknl.piutang-negara.permohonan-masuk.assign-job",
                "uses"  => "AssignJob@view"
            ]);

            Route::post('/assign-job/submit',[
                "as"    => "internal-kpknl.piutang-negara.permohonan-masuk.assign-job.submit",
                "uses"  => "AssignJob@submit"
            ]);
        });

        Route::group(['namespace'=>'DalamProses','prefix'=>'dalam-proses'], function(){
            Route::get('/',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.index",
                "uses"  => "Index@view"
            ]);

            Route::get('/current-step',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.current-step",
                "uses"  => "CurrentStep@view"
            ]);

            Route::post('/current-step/submit',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.current-step.submit",
                "uses"  => "CurrentStep@submit"
            ]);

            Route::get('/transfer-job',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.transfer-job",
                "uses"  => "TransferJob@view"
            ]);

            Route::post('/transfer-job/submit',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.transfer-job.submit",
                "uses"  => "TransferJob@submit"
            ]);

            Route::get('/job-done/submit',[
                "as"    => "internal-kpknl.piutang-negara.dalam-proses.job-done.submit",
                "uses"  => "JobDone@submit"
            ]);
        });
    });
});

//Ajax routes
Route::group(['namespace'=>'Ajax','prefix'=>'ajax'],function(){
     Route::post('/pegawai/getBySeksi',[
         "as"   => "ajax.pegawai.getBySeksi",
         "uses" => "Pegawai@getBySeksi",
     ]);
});

//Webhook routes
Route::group(['namespace'=>'Webhook','prefix'=>'webhook'], function (){
    Route::group(['namespace'=>'Telegram','prefix'=>'telegram'], function(){

        //Setup webhook update
        Route::get('/940524254:AAGQjY4i-RRpIKbeHm7IpOsljvQXHhrBo_k/setup',[
            "uses"  => "SetWebhook@index"
        ]);

        Route::post('/940524254:AAGQjY4i-RRpIKbeHm7IpOsljvQXHhrBo_k/update',[
            "uses"  => "GetUpdate@index"
        ]);
    });
});
