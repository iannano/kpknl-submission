"use strict";

const webpack               = require("webpack");
const path                  = require("path");
const htmlWebpackPlugin     = require("html-webpack-plugin");
const miniCssExtractPlugin  = require("mini-css-extract-plugin");

const config = (env) =>
{
    return{
        entry: {
            "core": "./resources/assets/entry-point/core/app-vendor.js",
            "backend-main": "./resources/assets/entry-point/backend/app-main.js",
            "frontend-main": "./resources/assets/entry-point/frontend/app-main.js"
        },
        output: {
            path: path.resolve(__dirname,'../public/assets/'),
            filename: '[name].[chunkhash].min.js',
            publicPath: 'assets/'
        },
        module:{
            rules:[
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: miniCssExtractPlugin.loader,
                            options: {
                                // you can specify a publicPath here
                                // by default it uses publicPath in webpackOptions.output
                                publicPath: './',
                                hmr: process.env.NODE_ENV === 'development',
                            },
                        },
                        'css-loader',
                    ],
                },
                {
                    test: /\.(eot|ttf|woff|woff2|otf)$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: "fonts/[name].[ext]"
                    }
                },
                {
                    test: /\.(jpe?g|png|gif)$/i,
                    loaders: [
                        'file-loader?hash=sha512&digest=hex&name=images/[hash].[ext]',
                        'image-webpack-loader?bypassOnDebug'
                    ]
                },
                {
                    test: /\.svg$/,
                    use: [
                        'raw-loader'
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: "style-loader" // creates style nodes from JS strings
                        },
                        {
                            loader: "css-loader" // translates CSS into CommonJS
                        },
                        {
                            loader: "sass-loader" // compiles Sass to CSS
                        }]
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader"
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                'window.jQuery': 'jquery',
                'window.$': 'jquery',
            }),
            new miniCssExtractPlugin({
                filename: '[name].min.css',
                chunkFilename: '[id].css',
                ignoreOrder: false, // Enable to remove warnings about conflicting order
            }),
            new htmlWebpackPlugin({
                chunks: ['core','backend-main'],
                template: "./resources/assets/ejs-templates/backend/main-layout.ejs",
                filename: "../../resources/views/layout/backend/main-layout.blade.php",
                inject: false
            }),
            new htmlWebpackPlugin({
                chunks: ['core','frontend-main'],
                template: "./resources/assets/ejs-templates/frontend/main-layout.ejs",
                filename: "../../resources/views/layout/frontend/main-layout.blade.php",
                inject: false
            }),
            new htmlWebpackPlugin({
                chunks: ['core','backend-main'],
                template: "./resources/assets/ejs-templates/backend/login.ejs",
                filename: "../../resources/views/layout/backend/login.blade.php",
                inject: false
            }),
        ],
        optimization: {
            splitChunks: {
                chunks: 'async',
                minSize: 30000,
            }
        }
    };
};

module.exports = config;
