<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PicSatuanKerja extends Model
{
    protected $table = "pic_satuan_kerja";
    protected $primaryKey = "id";

    public function seksi()
    {
        return $this->belongsTo(Seksi::class,"id_seksi","id");
    }

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class,"id_pegawai","id");
    }

    public function satuanKerja()
    {
        return $this->belongsTo(SatuanKerja::class,"id_satuan_kerja","id");
    }
}
