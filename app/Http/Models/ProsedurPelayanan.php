<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProsedurPelayanan extends Model
{
    protected $table = "prosedur_pelayanan";
    protected $primaryKey = "id";
}
