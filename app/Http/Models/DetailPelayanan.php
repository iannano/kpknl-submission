<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPelayanan extends Model
{
    protected $table = "detail_pelayanan";
    protected $primaryKey = "id";

    public function pelayanan()
    {
        return $this->hasOne(Pelayanan::class,"id","id_pelayanan");
    }

    public function satuanKerja()
    {
        return $this->hasOne(SatuanKerja::class,"id","id_satuan_kerja");
    }

    public function pegawai()
    {
        return $this->hasOne(Pegawai::class,"id","id_pegawai");
    }

    public function detailProsedurPelayanan()
    {
        return $this->hasMany(DetailProsedurPelayanan::class,"id_detail_pelayanan","id");
    }
}
