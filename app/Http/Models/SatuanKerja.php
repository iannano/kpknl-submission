<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SatuanKerja extends Model
{
    protected $table = "satuan_kerja";
    protected $primaryKey = "id";
}
