<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "pegawai";
    protected $primaryKey = "id";

    public function seksi()
    {
        return $this->belongsTo(Seksi::class, "id_seksi","id");
    }
}
