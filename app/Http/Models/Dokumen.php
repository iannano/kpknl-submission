<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    protected $table = "dokumen";
    protected $primaryKey = "id";
}
