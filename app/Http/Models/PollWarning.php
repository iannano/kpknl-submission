<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PollWarning extends Model
{
    protected $table = "poll_warning";
    protected $primaryKey = "id";
}
