<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pelayanan extends Model
{
    protected $table = "pelayanan";
    protected $primaryKey = "id";

    public function seksi()
    {
        return $this->belongsTo(Seksi::class,'id_seksi','id');
    }
}
