<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DetailProsedurPelayanan extends Model
{
    protected $table = "detail_prosedur_pelayanan";
    protected $primaryKey = "id";

    public function prosedurPelayanan()
    {
        return $this->hasOne(ProsedurPelayanan::class,"id",'id_prosedur_pelayanan');
    }
}
