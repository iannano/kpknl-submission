<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Telegram extends Model
{
    protected $table = "telegram";
    protected $primaryKey = "id";
}
