<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DokumenPelayanan extends Model
{
    protected $table = "dokumen_pelayanan";
    protected $primaryKey = "id";
}
