<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\Pegawai;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Support\Facades\DB;

class PegawaiRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;
    public function __construct()
    {
        $this->model = new Pegawai();
        $this->primaryKey = $this->model->getKeyName();
    }

    public function getPegawaiByTelegram($username)
    {
        return DB::select("SELECT id FROM pegawai WHERE telegram = $username");
    }
}
