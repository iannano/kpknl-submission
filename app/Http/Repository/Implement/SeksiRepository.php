<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\Seksi;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Database\Eloquent\Model;

class SeksiRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;

    public function __construct()
    {
        $this->model = new Seksi();
        $this->primaryKey = $this->model->getKeyName();
    }
}
