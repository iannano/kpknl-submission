<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\PollWarning;
use App\Http\Repository\Contract\QueryBaseFactory;

class PollWarningRepository extends QueryBaseFactory
{
    /**
     * @var
     */
    protected $model;

    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * ProsedurPelayananRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new PollWarning();
        $this->primaryKey = $this->model->getKeyName();
    }
}
