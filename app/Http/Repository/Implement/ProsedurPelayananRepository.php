<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\ProsedurPelayanan;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProsedurPelayananRepository
 * @package App\Http\Repository\Implement
 */
class ProsedurPelayananRepository extends QueryBaseFactory
{
    /**
     * @var ProsedurPelayanan
     */
    protected $model;

    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * ProsedurPelayananRepository constructor.
     */
    public function __construct()
    {
        $this->model = new ProsedurPelayanan();
        $this->primaryKey = $this->model->getKeyName();
    }
}
