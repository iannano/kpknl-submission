<?php
namespace App\Http\Repository\Implement;


use App\Http\Models\DetailProsedurPelayanan;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Support\Facades\DB;

class DetailProsedurPelayananRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;

    public function __construct()
    {
        parent::__construct();
        $this->model = new DetailProsedurPelayanan();
        $this->primaryKey = $this->model->getKeyName();
    }

    public function deleteByIdDetailPelayananAndIdProsedurPelayanan($idDetailPelayanan, $idProsedurPelayanan)
    {
        return DB::delete("DELETE FROM detail_prosedur_pelayanan WHERE id_detail_pelayanan = $idDetailPelayanan
        AND id_prosedur_pelayanan = $idProsedurPelayanan");
    }
}
