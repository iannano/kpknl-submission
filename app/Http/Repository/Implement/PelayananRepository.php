<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\Pelayanan;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PelayananRepository
 * @package App\Http\Repository\Implement
 */
class PelayananRepository extends QueryBaseFactory
{
    /**
     * @var Pelayanan
     */
    protected $model;
    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * PelayananRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Pelayanan();
        $this->primaryKey = $this->model->getKeyName();
    }
}
