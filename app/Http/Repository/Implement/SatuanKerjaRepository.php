<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\SatuanKerja;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SatuanKerjaRepository
 * @package App\Http\Repository\Implement
 */
class SatuanKerjaRepository extends QueryBaseFactory
{
    /**
     * @var SatuanKerja
     */
    protected $model;

    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * SatuanKerjaRepository constructor.
     */
    public function __construct()
    {
        $this->model = new SatuanKerja();
        $this->primaryKey = $this->model->getKeyName();
    }
}
