<?php


namespace App\Http\Repository\Implement;

use App\Http\Models\Dokumen;
use App\Http\Repository\Contract\QueryBaseFactory;

class DokumenRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;
    public function __construct()
    {
        $this->model = new Dokumen();
        $this->primaryKey = $this->model->getKeyName();
    }
}
