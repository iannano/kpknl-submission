<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\Telegram;
use App\Http\Repository\Contract\QueryBaseFactory;

class TelegramRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Telegram();
        $this->primaryKey = $this->model->getKeyName();
    }
}
