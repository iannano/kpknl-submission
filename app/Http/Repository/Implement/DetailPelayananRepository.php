<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Contract\QueryBaseFactory;
use Illuminate\Support\Facades\DB;

class DetailPelayananRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;

    public function __construct()
    {
        parent::__construct();
        $this->model = new DetailPelayanan();
        $this->primaryKey = $this->model->getKeyName();
    }

    public function findDetailPelayananBySeksi($idSeksi)
    {
        return DB::select("SELECT pegawai.nama, detail_pelayanan.id, pelayanan.pelayanan, detail_pelayanan.created_at, detail_pelayanan.waktu_berakhir, 
        detail_pelayanan.tiket, satuan_kerja.satuan_kerja 
        FROM detail_pelayanan
        JOIN pelayanan ON pelayanan.id = detail_pelayanan.id_pelayanan
        JOIN satuan_kerja ON satuan_kerja.id = detail_pelayanan.id_satuan_kerja
        LEFT JOIN pegawai ON pegawai.id = detail_pelayanan.id_pegawai
        WHERE pelayanan.id_seksi = $idSeksi
        ORDER BY id 
        DESC");
    }

    public function dataTiketPelayanan($ticket)
    {
        return DB::select("SELECT pelayanan.pelayanan, detail_pelayanan.tiket, satuan_kerja.satuan_kerja, satuan_kerja.email, satuan_kerja.chat_id, pelayanan.created_at
        FROM detail_pelayanan
        JOIN pelayanan ON pelayanan.id = detail_pelayanan.id_pelayanan
        JOIN satuan_kerja ON satuan_kerja.id = detail_pelayanan.id_satuan_kerja
        WHERE detail_pelayanan.tiket = $ticket AND id_detail_pelayanan = 0");
    }

    public function trackingLastUpdate($idDetailPelayanan)
    {
        return DB::select("SELECT prosedur_pelayanan.prosedur, detail_pelayanan.tiket, satuan_kerja.chat_id, satuan_kerja.satuan_kerja FROM detail_prosedur_pelayanan
        INNER JOIN detail_pelayanan ON detail_pelayanan.id = detail_prosedur_pelayanan.id_detail_pelayanan
        INNER JOIN prosedur_pelayanan ON prosedur_pelayanan.id = detail_prosedur_pelayanan.id_prosedur_pelayanan
        INNER JOIN satuan_kerja ON satuan_kerja.id = detail_pelayanan.id_satuan_kerja
        WHERE detail_pelayanan.id = $idDetailPelayanan
        ORDER BY detail_prosedur_pelayanan.id DESC LIMIT 1");
    }

    public function pollForWarning()
    {
        return DB::select("SELECT
            id_detail_pelayanan, nama,pelayanan, waktu_berakhir, waktu_sop, satuan_kerja, tiket
        FROM
            detail_pelayanan
        INNER JOIN
            pelayanan on detail_pelayanan.id_pelayanan = pelayanan.id
        INNER JOIN
            satuan_kerja ON satuan_kerja.id = detail_pelayanan.id_satuan_kerja
        INNER JOIN
            pegawai ON pegawai.id = detail_pelayanan.id_pegawai
        WHERE
            detail_pelayanan.status != 3 AND detail_pelayanan.waktu_berakhir IS NOT NULL;
        ");
    }
}
