<?php


namespace App\Http\Repository\Implement;


use App\Http\Models\PicSatuanKerja;
use App\Http\Models\Seksi;
use App\Http\Repository\Contract\QueryBaseFactory;

class PicSatuanKerjaRepository extends QueryBaseFactory
{
    protected $model;
    protected $primaryKey;

    public function __construct()
    {
        $this->model = new PicSatuanKerja();
        $this->primaryKey = $this->model->getKeyName();
    }
}
