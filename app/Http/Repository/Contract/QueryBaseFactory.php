<?php


namespace App\Http\Repository\Contract;


use Illuminate\Database\Eloquent\Model;

abstract class QueryBaseFactory
{
    /**
     * @var int $primaryKey
     */
    protected $primaryKey;

    /**
     * @var Model $model
     */
    protected $model;


    /**
     * QueryBaseFactory constructor.
     */
    public function __construct()
    {

    }

    /**
     * TODO: Find how to join with specific output. (Not Select All)
     * Use to find all record data
     * @param array|null $select
     * @param array|null $relations
     * @param null $orderBy
     * @param string $orderDirection
     * @return $this|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|Model|static|static[]
     */
    public function findAll(array $select = null, array $relations = null, $orderBy = null, $orderDirection = "DESC")
    {
        $data = $this->model;
        if($select != null)
        {
            //If select args not null
            $data = $data->select($select);
        }
        else
        {
            //If select args null
            $data = $data->select("*");
        }
        if($relations != null)
        {
            //Relation with another model
            $data = $data->with($relations);
        }

        if($orderBy != null)
        {
            //Ordering data
            $data = $data->orderBy($orderBy, $orderDirection);
        }

        $data = $data->get();

        return $data;
    }

    /**
     * Use to find record base by id record
     *
     * @param array|null $select
     * @param array|null $relations
     * @param $id
     * @return $this|\Illuminate\Database\Eloquent\Builder|Model|null|static
     */
    public function findById(array $select = null, array $relations = null, $id)
    {
        $data = $this->model;
        if($select != null)
        {
            //If select args not null
            $data = $data->select($select);
        }
        else
        {
            //If select args null
            $data = $data->select("*");
        }

        if($relations != null)
        {
            //Relation with another model
            $data = $data->with($relations);
        }
        $data = $data->where("id", $id)
            ->first();

        return $data;
    }

    /**
     * Use to find record base by params
     *
     * @param array|null $select
     * @param array $param
     * @param array|null $relations
     * @param null $orderBy
     * @param string $orderDirection
     * @return $this|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|Model|static|static[]
     */
    public function findByParam(array $select = null, array $param, array $relations = null, $orderBy = null, $orderDirection = "DESC")
    {
        $data = $this->model;
        if($select != null)
        {
            //If select args not null
            $data = $data->select($select);
        }
        else
        {
            //If select args null
            $data = $data->select("*");
        }

        foreach ($param as $key=>$value)
        {
            //Where condition
            $data = $data->where($key, $value);
        }

        if($relations != null)
        {
            //Relation with another model
            $data = $data->with($relations);
        }

        if($orderBy != null)
        {
            //Ordering data
            $data = $data->orderBy($orderBy, $orderDirection);
        }

        $data = $data->get();
        return $data;
    }

    /**
     * Use to save new record if primary key null or update record if primary key not null
     *
     * @param Model $model
     * @param null $id
     * @return Model|mixed
     */
    public function save(Model $model, $id = null)
    {
        if($id == null)
        {
            //Create new record
            $model->save();
            return $model->{$this->primaryKey};
        }
        else
        {
            //Update record
            $model->where("id", $id);
            $model->save();
            return $model->{$this->primaryKey};
        }
    }

    /**
     * @param $id
     * @return int
     * @throws \Exception
     */
    public function delete($id)
    {
        $data = $this->findById(array(), array(),$id);
        $data->delete();

        return 1;
    }
}
