<?php

namespace App\Http\Middleware\app;

use Closure;

class CheckAuthInternalKPKNL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->username == "" && $request->password == "")
        {
            return redirect()
                ->route("internal-kpknl.auth.login")
                ->with("error","Username Atau Password Tidak Boleh Kosong");
        }
        return $next($request);
    }
}
