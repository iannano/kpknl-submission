<?php

namespace App\Http\Middleware\app;

use Closure;

class CheckAuthSatuanKerja
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->username == "" && $request->password =="")
        {
            return redirect()
                ->route("login")
                ->with("error","Username dan password tidak boleh kosong");
        }
        return $next($request);
    }
}
