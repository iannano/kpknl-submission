<?php


namespace App\Http\Controllers\Webhook\Telegram;


use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\DetailProsedurPelayananRepository;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class TrackCommand extends Command
{
    protected $name = "lacak";
    protected $description = "Gunakan /lacak NomorTiket untuk melakukan pelacakan status terakhir pengajuan anda";
    protected $detailPelayananRepository;
    protected $detailProsedurPelayanan;

    public function handle($arguments)
    {
        $this->replyWithChatAction(["action"=>Actions::TYPING]);
        if($arguments == "")
        {
            $this->replyWithMessage(["text"=>"Nomor Tiket Harus Disertakan Dengan Perintah /lacak. Contoh (/lacak 12345)"]);
        }

        if($arguments != "")
        {
            $this->detailPelayananRepository = new DetailPelayananRepository();
            $this->detailProsedurPelayanan = new DetailProsedurPelayananRepository();
            $ticketCode = $arguments;
            $listDetailPelayanan = $this->detailPelayananRepository->findByParam(
                NULL,
                ['tiket'=>$ticketCode],
                ["pegawai","satuankerja","pelayanan"]
            );
            $listProsedur = $this->detailProsedurPelayanan->findByParam(
                NULL,
                ["id_detail_pelayanan"=>$listDetailPelayanan[0]['id']],
                ["prosedurpelayanan"],
                "id",
                "DESC"
            );
            $response = "Nama Pemohon: <b>".$listDetailPelayanan[0]['satuankerja']['satuan_kerja']."</b>\n";
            $response .= "Tanggal Pengajuan Permohonan: <b>".$listDetailPelayanan[0]['created_at']."</b>";
            if((int)count($listProsedur) === 0)
            {
                $response .= "<pre>Permohonan Belum Di Proses</pre>";
            }
            if((int)count($listProsedur) !== 0)
            {
                $response .= "<pre> Proses Saat Ini: \n</pre>";
                $response .= "<pre>".$listProsedur[0]['prosedurpelayanan']['prosedur']."</pre>";
            }
            $this->replyWithMessage(["text"=>$response,"parse_mode"=>"html"]);
        }
    }

}
