<?php

namespace App\Http\Controllers\Webhook\Telegram;

use App\Http\Models\Pegawai;
use App\Http\Models\SatuanKerja;
use App\Http\Models\Telegram;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use App\Http\Repository\Implement\TelegramRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class GetUpdate extends Controller
{
    protected $telegramModel;
    protected $pegawaiModel;
    protected $satuanKerjaModel;
    protected $telegramRepository;
    protected $pegawaiRepository;
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->telegramModel = new Telegram();
        $this->pegawaiModel = new Pegawai();
        $this->satuanKerjaModel = new SatuanKerja();
        $this->telegramRepository = new TelegramRepository();
        $this->pegawaiRepository = new PegawaiRepository();
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function index()
    {
        try
        {
            $updates = \Telegram\Bot\Laravel\Facades\Telegram::commandsHandler(true);
            $rawText = json_decode($updates);
            $this->telegramModel->payload = $updates;
            $this->telegramRepository->save($this->telegramModel);

            //Set chat id dalam table satuan_kerja
            $satuanKerjaModel = $this->satuanKerjaModel->where("telegram", $rawText->message->chat->username)->first();
            Log::debug("Satuan Kerja Model: ".json_encode($satuanKerjaModel));
            if($satuanKerjaModel)
            {
                $satuanKerjaModel->chat_id = $rawText->message->chat->id;;
                $this->satuanKerjaRepository->save($satuanKerjaModel, $satuanKerjaModel->id);
            }

            //Set chat id dalam table pegawai
            $pegawaiModel = $this->pegawaiModel->where("telegram", $rawText->message->chat->username)->first();
            Log::debug("Pegawai Model: ".json_encode($pegawaiModel));
            if($pegawaiModel)
            {
                $pegawaiModel->chat_id = $rawText->message->chat->id;
                $this->pegawaiRepository->save($pegawaiModel, $pegawaiModel->id);

            }
        }
        catch (\Exception $exception)
        {
            error_log("Exception catch: ".$exception);
        }
    }
}
