<?php


namespace App\Http\Controllers\Webhook\Telegram;


use App\Http\Repository\Implement\DetailPelayananRepository;
use Telegram\Bot\Laravel\Facades\Telegram;

class NotificationTimPenilai
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function sent($detailPelayananId)
    {
        $dataDetailPelayanan = $this->detailPelayananRepository->trackingLastUpdate($detailPelayananId);
        $response = "Hai! <b>".$dataDetailPelayanan[0]->satuan_kerja."</b> \n";
        $response .= "Pelayanan yang Bapak/Ibu ajukan membutuhkan penilaian terlebih dahulu \n";
        $response .= "Kami akan menugaskan Tim Penilai terbaik kami untuk melakukan penilaian tersebut \n";
        $response .= "Terima kasih. :) \n \n";
        $response .= "KPKNL Biak Menuju Industry 4.0";
        Telegram::sendMessage([
            "chat_id"       => $dataDetailPelayanan[0]->chat_id,
            "text"          => $response,
            "parse_mode"    => "html"
        ]);
    }
}
