<?php


namespace App\Http\Controllers\Webhook\Telegram;


use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class NotificationSubmission
{
    protected $satuanKerjaRepository;
    protected $detailPelayananRepository;
    public function __construct()
    {
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function send($ticket)
    {
        $dataDetailPelayanan = $this->detailPelayananRepository->dataTiketPelayanan($ticket);
        $response = "Hai! <b>".$dataDetailPelayanan[0]->satuan_kerja."</b> \n";
        $response .= "Terima kasih telah melakukan pengajuan pelayanan pada KPKNL Biak \n";
        $response .= "Dibawah ini merupakan kode pelacakan yang dapat Bapak/Ibu gunakan untuk mengetahui perkembangan terakhir pengajuan Bapak/Ibu \n \n";
        $response .= "Nomor Tiket Pelayanan: <b>".$ticket."</b> \n \n";
        $response .= "Pelacakan dapat menggunakan perintah /lacak ".$ticket." ataupun menggunakan <a href='https://kpknl-submission.rebillis.my.id/pelacakan'>Link Ini</a> \n \n";
        $response .= "KPKNL Biak Menuju Industry 4.0";
        Telegram::sendMessage([
            "chat_id"       => $dataDetailPelayanan[0]->chat_id,
            "text"          => $response,
            "parse_mode"    => "html"
        ]);
    }
}
