<?php


namespace App\Http\Controllers\Webhook\Telegram;


use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Telegram\Bot\Laravel\Facades\Telegram;

class NotificationProgress
{
    protected $satuanKerjaRepository;
    protected $detailPelayananRepository;
    public function __construct()
    {
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function send($detailPelayananId)
    {
        $dataDetailPelayanan = $this->detailPelayananRepository->trackingLastUpdate($detailPelayananId);

        $chatId = "";
        $response = "Hai! <b>".$dataDetailPelayanan[0]->satuan_kerja."</b> \n";
        $response .= "Pelayanan yang Bapak/Ibu ajukan, mengalami perubahan status. \n \n";
        $response .= "Status Terakhir : ".$dataDetailPelayanan[0]->prosedur ."\n \n";
        $response .= "Bapak/Ibu dapat melakukan pengecekan menggunakan perintah /lacak ".$dataDetailPelayanan[0]->tiket." atau menggunakan <a href='https://kpknl-submission.rebillis.my.id/pelacakan'>Link ini</a> \n \n";
        $response .= "KPKNL Biak Menuju Industry 4.0";
        Telegram::sendMessage([
            "chat_id"       => $dataDetailPelayanan[0]->chat_id,
            "text"          => $response,
            "parse_mode"    => "html"
        ]);
    }
}
