<?php


namespace App\Http\Controllers\Webhook\Telegram;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartCommand extends Command
{
    protected $name = "start";
    protected $description = "Mulai Interaksi";

    public function handle($arguments)
    {
        $this->replyWithChatAction(["action"=>Actions::TYPING]);
        $this->replyWithMessage(["text"=>"Hai! Ada yang bisa kami bantu?"]);
        $commands = $this->getTelegram()->getCommands();
        $response = '';
        foreach ($commands as $name => $command) {
            $response .= sprintf('/%s - %s' . PHP_EOL, $name, $command->getDescription());
        }
        $this->replyWithMessage(['text' => $response]);
    }
}
