<?php

namespace App\Http\Controllers\Webhook\Telegram;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class SetWebhook extends Controller
{
    public function index()
    {
        try
        {
            Telegram::setWebhook(
                ["url"=>"https://kpknlbiak.id/webhook/telegram/940524254:AAGQjY4i-RRpIKbeHm7IpOsljvQXHhrBo_k/update"]
            );
            echo "Webhook Set!";
        }
        catch (\Exception $exception)
        {
            error_log("Exception catch: ".$exception);
            echo "Error Log: ".$exception;
        }
    }
}
