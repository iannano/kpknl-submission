<?php


namespace App\Http\Controllers\Factory;



use Illuminate\Http\Request;

interface EndpointController
{
    public function __construct();
    public function submit(Request $request);
}
