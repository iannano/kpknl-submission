<?php


namespace App\Http\Controllers\Factory;


use Illuminate\Http\Request;

/**
 * Interface ViewController
 * @package App\Http\Controllers\Factory
 */
interface ViewController
{
    /**
     * ViewController constructor.
     */
    public function __construct();

    /**
     * Use for show view only
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request);
}
