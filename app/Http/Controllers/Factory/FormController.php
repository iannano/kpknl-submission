<?php
namespace App\Http\Controllers\Factory;

use Illuminate\Http\Request;

/**
 * Interface FormController
 * @package App\Http\Controllers\Factory
 */
interface FormController
{
    /**
     * FormController constructor.
     */
    public function __construct();

    /**
     * Use for show form view
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request);

    /**
     * Use for submit data from form
     * @param Request $request
     * @return mixed
     */
    public function submit(Request $request);
}
