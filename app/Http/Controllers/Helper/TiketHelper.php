<?php


namespace App\Http\Controllers\Helper;


use App\Http\Controllers\Helper\Factory\TiketFactory;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class TiketHelper implements TiketFactory
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function createTicket($idPelayanan, $idSatuanKerja)
    {
        $tanggalPermohonan = str_replace("-","",Carbon::now()->toDateString());
        $totalTicket = $this->findTicketByPelayananAndSatuanKerja($idPelayanan, $idSatuanKerja)+1;
        $ticket = $tanggalPermohonan.$idPelayanan.$idSatuanKerja.$totalTicket;

        return $ticket;
    }

    public function findTicketByCodeTicket($ticket)
    {
        // TODO: Implement findTicketByCodeTicket() method.
    }

    public function findTicketByPelayananAndSatuanKerja($idPelayanan, $idSatuanKerja)
    {
        $totalTicket = count($this->detailPelayananRepository->findByParam(
            NULL,
            ["id_pelayanan"=>$idPelayanan, "id_satuan_kerja"=>$idSatuanKerja]
        ));

        return $totalTicket;
    }
}