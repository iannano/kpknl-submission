<?php


namespace App\Http\Controllers\Helper\Factory;


interface TiketFactory
{
    public function __construct();
    public function createTicket($idPelayanan, $idSatuanKerja);
    public function findTicketByCodeTicket($ticket);
    public function findTicketByPelayananAndSatuanKerja($idPelayanan, $idSatuanKerja);
}