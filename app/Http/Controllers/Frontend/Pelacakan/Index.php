<?php

namespace App\Http\Controllers\Frontend\Pelacakan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\DetailProsedurPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\DetailProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller implements FormController
{
    protected $detailPelayananRepository;
    protected $detailProsedurPelayanan;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->detailProsedurPelayanan = new DetailProsedurPelayananRepository();
    }

    public function view(Request $request)
    {
        return view("frontend.pelacakan.index");
    }

    public function submit(Request $request)
    {
        $ticketCode = $request->post("ticket-code");
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findByParam(
            NULL,
            ['tiket'=>$ticketCode],
            ["pegawai","satuankerja","pelayanan"]
        );
        $data['listProsedur'] = $this->detailProsedurPelayanan->findByParam(
            NULL,
            ["id_detail_pelayanan"=>$data['listDetailPelayanan'][0]['id']],
            ["prosedurpelayanan"]
        );
        return view("frontend.pelacakan.hasil-pelacakan", $data);
    }
}
