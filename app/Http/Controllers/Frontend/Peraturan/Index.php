<?php

namespace App\Http\Controllers\Frontend\Peraturan;

use App\Http\Controllers\Factory\ViewController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller implements ViewController
{
    public function __construct()
    {
    }

    public function view(Request $request)
    {
        return view("frontend.peraturan.index");
    }

}
