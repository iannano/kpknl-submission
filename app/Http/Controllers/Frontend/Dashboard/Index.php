<?php

namespace App\Http\Controllers\Frontend\Dashboard;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Index extends Controller implements ViewController
{
    protected $detailPelayananRepository;
    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findByParam(
            NULL,
            ["id_satuan_kerja"=>session('access-satuan-kerja')['id'],"id_detail_pelayanan"=>0],
            NULL,
            "id",
            "DESC"
        );
        return view("frontend.dashboard.index",$data);
    }
}
