<?php

namespace App\Http\Controllers\Frontend\PengajuanPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Pelayanan extends Controller implements FormController
{
    protected $detailPelayananModel;
    protected $pelayananRepository;
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->pelayananRepository  = new PelayananRepository();
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $data["listPelayanan"] = $this->pelayananRepository->findByParam(
            ["id","pelayanan"],
            ["jenis"=>"external"]
        );
        return view("frontend.pengajuan-pelayanan.pelayanan", $data);
    }

    public function submit(Request $request)
    {
        $idPelayanan = $request->post("id_pelayanan");
        $jenisPengajuan = 0;

        $request->session()->put('pelayanan-frontend', $idPelayanan);

        if($jenisPengajuan == "0")
        {
            //Offline submission (Datang ke KPKNL langsung)
            return redirect()
                ->route("pengajuan-pelayanan.tiket")
                ->with("code",0);
        }
        else
        {
            //Online submission (Upload langsung)
            return redirect()
                ->route("pengajuan-pelayanan.dokumen");
        }
    }
}
