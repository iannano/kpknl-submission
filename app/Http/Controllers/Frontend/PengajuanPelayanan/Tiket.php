<?php

namespace App\Http\Controllers\Frontend\PengajuanPelayanan;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Controllers\Helper\TiketHelper;
use App\Http\Controllers\Webhook\Telegram\NotificationSubmission;
use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\PicSatuanKerjaRepository;
use App\Mail\TiketPelayanan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;

class Tiket extends Controller implements ViewController
{
    protected $detailPelayananModel;
    protected $detailPelayananRepository;
    protected $picSatuanKerjaRepository;
    protected $pelayananRepository;
    protected $ticketHelper;
    protected $notificationSubmission;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->picSatuanKerjaRepository = new PicSatuanKerjaRepository();
        $this->pelayananRepository = new PelayananRepository();
        $this->ticketHelper = new TiketHelper();
        $this->notificationSubmission = new NotificationSubmission();
    }

    public function view(Request $request)
    {
        if(session('code') == 0)
        {
            $result = $this->saveDetailPelayananOffline($request);
            if($result !== "error")
            {
                //Success and send ticket
                $data["dataTicketCode"] = $result;
            }
            else
            {
                //Error
            }
        }
        else
        {
            //Permohonan pelayanan secara online
            echo "Hit2";
        }
        return view('frontend.pengajuan-pelayanan.tiket-pengajuan', $data);
    }

    private function saveDetailPelayananOffline(Request $request)
    {
        //Permohonan pelayanan secara offline
        $idSatuanKerja = $request->session()->get('access-satuan-kerja')['id'];
        $idPelayanan = $request->session()->get('pelayanan-frontend');

        //Create ticket number
        $ticket = $this->ticketHelper->createTicket($idPelayanan, $idSatuanKerja);

        //Get data pelayanan
        $dataPelayanan = $this->pelayananRepository->findById(
            NULL,
            NULL,
            ['id'=>$idPelayanan]
        );

        //TODO: Uncomment jika seluruh seksi sudah melakukan pembagian satuan kerja
        //Get pic satuan kerja
        //$data['picSatuanKerja'] = $this->picSatuanKerjaRepository->findByParam(
        //    NULL,
        //    ["id_satuan_kerja"=>$idSatuanKerja, "id_seksi"=>$dataPelayanan['id_seksi']]
        //);

        //$idPegawai = $data['picSatuanKerja'][0]['id_pegawai'];

        $this->detailPelayananModel->id_pelayanan = $idPelayanan;
        $this->detailPelayananModel->id_satuan_kerja = $idSatuanKerja;

        //TODO: Uncomment jika seluruh seksi sudah melakukan pembagian satuan kerja
        //$this->detailPelayananModel->id_pegawai = $idPegawai;
        $this->detailPelayananModel->tiket = $ticket;
        $this->detailPelayananModel->id_detail_pelayanan = 0;
        $this->detailPelayananModel->status = 0;

        $request = $this->detailPelayananRepository->save($this->detailPelayananModel);
        if($request != "")
        {
            $this->notificationSubmission->send($ticket);
            $this->sendEmail($ticket);
            return $ticket;
        }
        else
        {
            return "error";
        }
    }

    private function saveDetailPelayananOnline(Request $request)
    {

    }

    private function sendEmail($tiket)
    {
        $data['dataDetailPelayanan'] = $this->detailPelayananRepository->dataTiketPelayanan($tiket);
        Mail::to($data['dataDetailPelayanan'][0]->email)
            ->send(new TiketPelayanan($data));
    }
}
