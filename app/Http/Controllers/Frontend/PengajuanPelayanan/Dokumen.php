<?php

namespace App\Http\Controllers\Frontend\PengajuanPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Dokumen extends Controller implements FormController
{
    protected $pelayananRepository;
    protected $dokumenRepository;

    public function __construct()
    {
        $this->pelayananRepository = new PelayananRepository();
        $this->dokumenRepository = new DokumenRepository();
    }

    public function view(Request $request)
    {
        $data["listDokumen"] = $this->dokumenRepository->findByParam(
            NULL,
            ['id_pelayanan'=>Redis::get('id_pelayanan')]
        );
        $data["dataPelayanan"] = $this->pelayananRepository->findById(
            NULL,
            NULL,
            ["id"=>Redis::get('id_pelayanan')]);
        return view("frontend.pengajuan-pelayanan.dokumen", $data);
    }

    public function submit(Request $request)
    {
        return view("frontend.pengajuan-pelayanan.tiket-pengajuan");
    }
}
