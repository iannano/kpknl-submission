<?php

namespace App\Http\Controllers\Frontend\Pelayanan;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Detail extends Controller implements ViewController
{
    protected $seksiRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->seksiRepository = new SeksiRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        $slugseksi = $request->route("slugseksi");
        if($slugseksi)
        {
            //Get id seksi terpilih base by slug
            $dataSeksi = $this->seksiRepository->findByParam(
                ["id"],
                ["slug"=>$slugseksi]
            );

            //Prepare data for ui/ux
            $data['listPelayanan'] = $this->pelayananRepository->findByParam(
                ["id","pelayanan","slug","waktu_sop","jenis","status"],
                ["id_seksi"=>$dataSeksi[0]->id]
            );
            $data['slugseksi'] = $slugseksi;
            return view("frontend.pelayanan.detail", $data);
        }
        else
        {
            //When slug not found
            redirect(404);
        }
    }
}
