<?php

namespace App\Http\Controllers\Frontend\Pelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;

class Prosedur extends Controller implements FormController
{
    protected $pelayananRepository;
    protected $prosedurPelayananRepository;
    protected $dokumenRepository;

    public function __construct()
    {
        $this->pelayananRepository = new PelayananRepository();
        $this->prosedurPelayananRepository = new ProsedurPelayananRepository();
        $this->dokumenRepository = new DokumenRepository();
    }

    public function view(Request $request)
    {
        $slugpelayanan = $request->route("slugpelayanan");
        $data["dataPelayanan"]= $this->pelayananRepository->findByParam(
            NULL,
            ["slug"=>$slugpelayanan],
            ["seksi"]
        );

        $data["listProsedurPelayanan"] = $this->prosedurPelayananRepository->findByParam(
            ["id","prosedur"],
            ["id_pelayanan"=>$data["dataPelayanan"][0]->id]);

        $data["listDokumen"] = $this->dokumenRepository->findByParam(
            ["id", "dokumen"],
            ["id_pelayanan"=>$data["dataPelayanan"][0]->id]
        );

        return view("frontend.pelayanan.prosedur", $data);
    }

    public function submit(Request $request)
    {
        $idPelayanan = Crypt::decrypt($request->post("idPelayanan"));
        $dataPelayanan = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);

        //Set Redis Command
        Redis::command("SET",["id_pelayanan",$idPelayanan]);

        return redirect()
            ->route("pengajuan-pelayanan.index")
            ->with("information","Persiapan Pengajuan Pelayanan".$dataPelayanan->pelayanan);
    }
}
