<?php

namespace App\Http\Controllers\Frontend\Pelayanan;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class Index
 * @package App\Http\Controllers\Frontend\Pelayanan
 */
class Index extends Controller implements ViewController
{
    /**
     * @var SeksiRepository
     */
    protected $seksiRepository;

    /**
     * Index constructor.
     */
    public function __construct()
    {
        $this->seksiRepository              = new SeksiRepository();
        $this->pelayananRepository          = new PelayananRepository();
        $this->prosedurPelayananRepository  = new ProsedurPelayananRepository();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function view(Request $request)
    {
        $data['listSeksi'] = $this->seksiRepository->findAll();
        return view("frontend.pelayanan.index", $data);
    }
}
