<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Factory\FormController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ForgetPassword extends Controller implements FormController
{
    public function __construct()
    {
    }

    public function view(Request $request)
    {
        return view("frontend.auth.forget-password");
    }

    public function submit(Request $request)
    {
        // TODO: Implement submit() method.

        // Dummy send email function
        $this->sendEmail();
    }

    private function sendEmail($to = "")
    {
        $log = Mail::to("ibnuauliana@gmail.com")
            ->send(new \App\Mail\ForgetPassword());

        dd($log);
    }
}
