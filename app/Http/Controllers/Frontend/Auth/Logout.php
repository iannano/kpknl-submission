<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Factory\ViewController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

/**
 * Class Logout
 * @package App\Http\Controllers\Frontend\Auth
 */
class Logout extends Controller implements ViewController
{
    /**
     * Logout constructor.
     */
    public function __construct()
    {
    }

    /**
     * Use for logout
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function view(Request $request)
    {
        $request->session()->forget('access-satuan-kerja');
        return redirect("/");
    }

}
