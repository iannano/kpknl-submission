<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Login extends Controller implements FormController
{
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        return view("frontend.auth.login");
    }

    public function submit(Request $request)
    {
        //Get post data
        $username   = $request->post("username");
        $password   = $request->post("password");

        //Find satuan kerja by submited username and password
        $dataSatuanKerja = $this->satuanKerjaRepository->findByParam(
            ["id","satuan_kerja"],
            ["username"=>$username, "password"=>sha1(md5($password))]
        );

        if(count($dataSatuanKerja)>0)
        {
            //Data satuan kerja exist
            //Save data id satuan kerja and satuan kerja to session
            $data = array(
                "id"    => $dataSatuanKerja[0]->id,
                "satuan_kerja"  => $dataSatuanKerja[0]->satuan_kerja
            );

            $request->session()->put("access-satuan-kerja", $data);

            return redirect()
                ->route("dashboard.index")
                ->with("greeting", "Selamat datang ".$dataSatuanKerja);
        }
        else
        {
            //Data satuan kerja not exist
            return redirect()
                ->route("login")
                ->with("error","Username Dan Password Tidak Ditemukan");
        }
    }


}
