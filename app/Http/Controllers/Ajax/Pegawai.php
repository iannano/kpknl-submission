<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Repository\Implement\PegawaiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Pegawai extends Controller
{
    protected $pegawaiRepository;

    public function __construct()
    {
        $this->pegawaiRepository = new PegawaiRepository();
    }

    public function getBySeksi(Request $request)
    {
        if($request->ajax())
        {
            //Ajax request
            $idSeksi = $request->post('idSeksi');
            $listPegawai = $this->pegawaiRepository->findByParam(
                ["id","nama"],
                ['id_seksi'=> $idSeksi]
            );

            return response()
                ->json($listPegawai);
        }
        else
        {
            return redirect(404);
        }
    }
}
