<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Pelayanan;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Create extends Controller implements FormController
{
    protected $pelayananModel;

    protected $seksiRepository;

    protected $pelayananRepository;

    public function __construct()
    {
        $this->pelayananModel       = new Pelayanan();
        $this->seksiRepository      = new SeksiRepository();
        $this->pelayananRepository  = new PelayananRepository();
    }

    public function view(Request $request)
    {
        $data["listSeksi"] = $this->seksiRepository->findAll(["id","seksi"]);
        return view("backend.administrator.manajemen-pelayanan.create", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idSeksi    = $request->post("seksi");
        $pelayanan  = $request->post("pelayanan");
        $slug       = \Illuminate\Support\Str::slug($pelayanan,"-");
        $waktuSOP   = $request->post("waktu_sop");
        $biaya      = $request->post("biaya");
        $jenis      = $request->post("jenis");
        $status     = $request->post("status");
        $keterangan = $request->post("keterangan");

        //Add to model
        $this->pelayananModel->id_seksi    = $idSeksi;
        $this->pelayananModel->pelayanan   = $pelayanan;
        $this->pelayananModel->slug        = $slug;
        $this->pelayananModel->waktu_sop   = $waktuSOP;
        $this->pelayananModel->biaya       = $biaya;
        $this->pelayananModel->jenis       = $jenis;
        $this->pelayananModel->status      = $status;
        $this->pelayananModel->keterangan  = $keterangan;

        //Save to database
        $result = $this->pelayananRepository->save($this->pelayananModel);
        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pelayanan.index")
                ->with("information","Data Pelayanan Berhasil Dibuat");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pelayanan.create")
                ->with("error","Terjadi kesalahan dalam proses data");
        }
    }
}
