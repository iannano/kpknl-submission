<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPelayanan;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Models\Seksi;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class Index
 * @package App\Http\Controllers\Backend\Administrator\ManajemenPelayanan
 */
class Index extends Controller implements ViewController
{
    protected $seksiModel;

    /**
     * @var PelayananRepository
     */
    protected $pelayananRepository;

    /**
     * Index constructor.
     */
    public function __construct()
    {
        $this->seksiModel           = new Seksi();
        $this->pelayananRepository  = new PelayananRepository();
    }

    /**
     * TODO: Add data from database
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function view(Request $request)
    {
        $data["listPelayanan"] = $this->pelayananRepository
            ->findAll(NULL,["seksi"]);
        return view("backend.administrator.manajemen-pelayanan.index", $data);
    }
}
