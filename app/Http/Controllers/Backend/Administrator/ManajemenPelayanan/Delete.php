<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $pelayananRepository;
    public function __construct()
    {
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        $id = Crypt::decrypt($request->get("id"));
        $data["dataPelayanan"] = $this->pelayananRepository->findById(NULL, NULL, $id);
        return view("backend.administrator.manajemen-pelayanan.delete", $data);
    }

    public function submit(Request $request)
    {
        $id = Crypt::decrypt($request->post("id"));
        $this->pelayananRepository->delete($id);
        return redirect()
            ->route("internal-kpknl.manajemen-pelayanan.index")
            ->with("information","Data berhasil dihapus");
    }
}
