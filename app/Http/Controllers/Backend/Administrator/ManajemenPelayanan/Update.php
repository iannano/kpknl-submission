<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Pelayanan;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $pelayananModel;
    protected $pelayananRepository;
    protected $seksiRepository;

    public function __construct()
    {
        $this->pelayananRepository  = new PelayananRepository();
        $this->seksiRepository      = new SeksiRepository();
        $this->pelayananModel       = new Pelayanan();
    }

    public function view(Request $request)
    {
        $idPelayanan = Crypt::decrypt($request->get("id"));
        $data["listSeksi"]      = $this->seksiRepository->findAll();
        $data["dataPelayanan"]  = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);
        return view("backend.administrator.manajemen-pelayanan.update", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $id         = Crypt::decrypt($request->post("id"));
        $idSeksi    = $request->post("seksi");
        $pelayanan  = $request->post("pelayanan");
        $slug       = \Illuminate\Support\Str::slug($pelayanan,"-");
        $waktuSOP   = $request->post("waktu_sop");
        $biaya      = $request->post("biaya");
        $jenis      = $request->post("jenis");
        $status     = $request->post("status");
        $keterangan = $request->post("keterangan");

        //Add to model
        $pelayananModel = $this->pelayananModel->find($id);
        $pelayananModel->id_seksi    = $idSeksi;
        $pelayananModel->pelayanan   = $pelayanan;
        $pelayananModel->slug        = $slug;
        $pelayananModel->waktu_sop   = $waktuSOP;
        $pelayananModel->biaya       = $biaya;
        $pelayananModel->jenis       = $jenis;
        $pelayananModel->status      = $status;
        $pelayananModel->keterangan  = $keterangan;

        //Update to database
        $result = $this->pelayananRepository->save($pelayananModel, $id);
        if($result != "")
        {
            //Update success
            return redirect()
                ->route("internal-kpknl.manajemen-pelayanan.index")
                ->with("information","Data Pelayanan Berhasil Diupdate");
        }
        else
        {
            //Update failed
            return redirect()
                ->route("internal-kpknl.manajemen-pelayanan.update", ["id"=>Crypt::encrypt($id)])
                ->with("error","Terjadi kesalahan dalam proses data");
        }
    }
}
