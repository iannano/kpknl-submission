<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPegawai;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Pegawai;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $pegawaiModel;
    protected $pegawaiRepository;
    protected $seksiRepository;

    public function __construct()
    {
        $this->pegawaiModel = new Pegawai();
        $this->pegawaiRepository = new PegawaiRepository();
        $this->seksiRepository = new SeksiRepository();
    }

    public function view(Request $request)
    {
        $idPegawai = Crypt::decrypt($request->get("id"));
        $data["listSeksi"] = $this->seksiRepository->findAll();
        $data["dataPegawai"] = $this->pegawaiRepository->findById(
            NULL,
            ['seksi'],
            ["id_pegawai"=>$idPegawai]
        );
        return view("backend.administrator.manajemen-pegawai.update", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPegawai = Crypt::decrypt($request->post('id-pegawai'));
        $role = $request->post('role');
        $nama = $request->post('nama');
        $username = $request->post('username');

        //Fill model with data
        $pegawaiModel = $this->pegawaiModel->find($idPegawai);
        $pegawaiModel->role = $role;
        $pegawaiModel->nama = $nama;
        $pegawaiModel->username = $username;

        //Execute query update
        $result = $this->pegawaiRepository->save($pegawaiModel, $idPegawai);

        //Check result
        if($result != "")
        {
            //Success
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.index")
                ->with("information","Update Pegawai Berhasil");
        }
        else
        {
            //Error
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.update",["id"=>Crypt::encrypt($idPegawai)])
                ->with("error","Terjadi Kesalahan Dalam Update Data Pegawai");
        }
    }

}
