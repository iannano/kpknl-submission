<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPegawai;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Pegawai;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Create extends Controller implements FormController
{
    protected $pegawaiModel;
    protected $seksiRepository;
    protected $pegawaiRepository;

    public function __construct()
    {
        $this->pegawaiModel = new Pegawai();
        $this->seksiRepository = new SeksiRepository();
        $this->pegawaiRepository = new PegawaiRepository();
    }

    public function view(Request $request)
    {
        $data['listSeksi'] = $this->seksiRepository->findAll();
        return view("backend.administrator.manajemen-pegawai.create", $data);
    }

    public function submit(Request $request)
    {
        $idSeksi    = $request->post("seksi");
        $role       = $request->post("role");
        $nama       = $request->post("nama");
        $username   = $request->post("username");
        $password   = sha1(md5($request->post("password")));

        $this->pegawaiModel->id_seksi = $idSeksi;
        $this->pegawaiModel->role = $role;
        $this->pegawaiModel->nama = $nama;
        $this->pegawaiModel->username = $username;
        $this->pegawaiModel->password = $password;

        $result = $this->pegawaiRepository->save($this->pegawaiModel);
        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.index")
                ->with("information","Simpan Data Pegawai Berhasil");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.create")
                ->with("error","Terjadi Kesalahan Dalam Penyimpanan Data");
        }
    }

}
