<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPegawai;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Models\Pegawai;
use App\Http\Repository\Implement\PegawaiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller implements ViewController
{
    protected $pegawaiModel;
    protected $pegawaiRepository;

    public function __construct()
    {
        $this->pegawaiModel = new Pegawai();
        $this->pegawaiRepository = new PegawaiRepository();
    }

    public function view(Request $request)
    {
        $data["listPegawai"] = $this->pegawaiRepository->findAll(NULL, ["seksi"]);
        return view("backend.administrator.manajemen-pegawai.index", $data);
    }
}
