<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPegawai;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\PegawaiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $pegawaiRepository;

    public function __construct()
    {
        $this->pegawaiRepository = new PegawaiRepository();
    }

    public function view(Request $request)
    {
        //Get url parameter
        $idPegawai = Crypt::decrypt($request->get("id"));

        //Get data pegawai
        $data["dataPegawai"] = $this->pegawaiRepository->findById(NULL, NULL, $idPegawai);

        //Show view with data
        return view("backend.administrator.manajemen-pegawai.delete", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPegawai = Crypt::decrypt($request->post("id-pegawai"));

        //Execute delete data pegawai
        $result = $this->pegawaiRepository->delete($idPegawai);

        //Check result
        if($result != "")
        {
            //Success
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.index")
                ->with("information","Hapus Data Pegawai Berhasil");
        }
        else
        {
            //Error
            return redirect()
                ->route("internal-kpknl.manajemen-pegawai.delete", ["id"=>Crypt::encrypt($idPegawai)])
                ->with("error","Terjadi Kesalahan Dalam Hapus Pegawai");
        }
    }

}
