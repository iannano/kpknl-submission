<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\SatuanKerja;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Create extends Controller implements FormController
{
    protected $satuanKerjaModel;
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->satuanKerjaModel = new SatuanKerja();
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        return view("backend.administrator.manajemen-satuan-kerja.create");
    }

    public function submit(Request $request)
    {
        $satuanKerja = $request->post("satuan-kerja");
        $email = $request->post("email");
        $phone = $request->post("phone");
        $username = $request->post("username");
        $password = $request->post("password");

        $this->satuanKerjaModel->satuan_kerja = $satuanKerja;
        $this->satuanKerjaModel->email = $email;
        $this->satuanKerjaModel->phone = $phone;
        $this->satuanKerjaModel->username = $username;
        $this->satuanKerjaModel->password = sha1(md5($password));

        $request = $this->satuanKerjaRepository->save($this->satuanKerjaModel);

        if($request != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.index")
                ->with("information","Simpan Satuan Kerja Sukses");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.create")
                ->with("error","Terjadi Kesalahan Dalam Menyimpan Data Satuan Kerja");
        }
    }

}
