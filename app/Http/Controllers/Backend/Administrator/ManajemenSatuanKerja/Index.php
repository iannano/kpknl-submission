<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenSatuanKerja;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller implements ViewController
{
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        $data['listSatuanKerja'] = $this->satuanKerjaRepository->findAll();
        return view("backend.administrator.manajemen-satuan-kerja.index", $data);
    }
}