<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        $idSatuanKerja = Crypt::decrypt($request->get('id'));
        $data['dataSatuanKerja'] = $this->satuanKerjaRepository->findById(NULL, NULL, $idSatuanKerja);
        return view('backend.administrator.manajemen-satuan-kerja.delete', $data);
    }

    public function submit(Request $request)
    {
        $idSatuanKerja = Crypt::decrypt($request->post('id'));
        $result = $this->satuanKerjaRepository->delete($idSatuanKerja);
        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.index")
                ->with("information",'Hapus Data Berhasil');
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.delete",['id'=>Crypt::encrypt($idSatuanKerja)])
                ->with("error","Terjadi Kesalahan Dalam Menghapus Data Satuan Kerja");
        }
    }

}
