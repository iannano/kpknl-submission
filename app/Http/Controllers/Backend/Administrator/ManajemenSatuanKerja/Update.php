<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\SatuanKerja;
use App\Http\Repository\Implement\SatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $satuanKerjaModel;
    protected $satuanKerjaRepository;

    public function __construct()
    {
        $this->satuanKerjaModel = new SatuanKerja();
        $this->satuanKerjaRepository = new SatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        $id = Crypt::decrypt($request->get("id"));
        $data['dataSatuanKerja'] = $this->satuanKerjaRepository->findById(NULL, NULL, $id);
        return view("backend.administrator.manajemen-satuan-kerja.update", $data);
    }

    public function submit(Request $request)
    {
        $idSatuanKerja = Crypt::decrypt($request->post('id'));
        $satuanKerja = $request->post('satuan-kerja');
        $email = $request->post('email');
        $phone = $request->post('phone');

        $satuanKerjaModel = $this->satuanKerjaModel->find($idSatuanKerja);
        $satuanKerjaModel->satuan_kerja = $satuanKerja;
        $satuanKerjaModel->email = $email;
        $satuanKerjaModel->phone = $phone;

        $result = $this->satuanKerjaRepository->save($satuanKerjaModel, $idSatuanKerja);
        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.index")
                ->with("information","Update Satuan Kerja Berhasil");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-satuan-kerja.update",["id"=>Crypt::encrypt($idSatuanKerja)])
                ->with("error","Terjadi Kesalahan Dalam Update Satuan Kerja");
        }
    }

}
