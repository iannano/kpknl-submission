<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenProsedur;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $prosedurRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->pelayananRepository = new PelayananRepository();
        $this->prosedurRepository = new ProsedurPelayananRepository();
    }

    public function view(Request $request)
    {
        $idProsedur = Crypt::decrypt($request->post("id"));

        $data["dataProsedur"] = $this->prosedurRepository->findById(NULL, NULL, $idProsedur);
        $data["dataPelayanan"] = $this->pelayananRepository->findById(NULL, NULL, $data["dataProsedur"]["id"]);

        return view("backend.administrator.manajemen-prosedur.delete", $data);
    }

    public function submit(Request $request)
    {
        $idProsedur = Crypt::decrypt($request->post("id_prosedur"));
        $idPelayanan = Crypt::decrypt($request->post("id_pelayanan"));
        $this->prosedurRepository->delete($idProsedur);
        return redirect()
            ->route("internal-kpknl.manajemen-prosedur.index", ["id"=>Crypt::encrypt($idPelayanan)])
            ->with("information","Hapus data berhasil");
    }
}
