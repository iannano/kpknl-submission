<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenProsedur;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Index extends Controller implements ViewController
{
    protected $prosedurRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->prosedurRepository = new ProsedurPelayananRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        //Get id in url parameter
        $idPelayanan = Crypt::decrypt($request->id);

        //Get data pelayanan base by id
        $data['dataPelayanan'] = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);

        $data['listProsedur'] = $this->prosedurRepository->findByParam(
            NULL,
            ["id_pelayanan"=>$idPelayanan]
        );

        return view('backend.administrator.manajemen-prosedur.index', $data);
    }
}
