<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenProsedur;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\ProsedurPelayanan;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Create extends Controller implements FormController
{
    protected $prosedurModel;
    protected $prosedurRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->prosedurModel = new ProsedurPelayanan();
        $this->prosedurRepository = new ProsedurPelayananRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        //Get id in url parameter
        $idPelayanan = Crypt::decrypt($request->id);

        //Get data pelayanan base by id
        $data['dataPelayanan'] = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);

        return view("backend.administrator.manajemen-prosedur.create", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPelayanan    = Crypt::decrypt($request->id_pelayanan);
        $prosedur       = $request->post("prosedur");
        $tahapan        = $request->post("tahapan");

        //Prefilled model with data
        $this->prosedurModel->id_pelayanan  = $idPelayanan;
        $this->prosedurModel->prosedur      = $prosedur;
        $this->prosedurModel->tahapan       = $tahapan;

        //Save data to database
        $result = $this->prosedurRepository->save($this->prosedurModel);

        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-prosedur.index", ["id" => Crypt::encrypt($idPelayanan)])
                ->with("information","Pembuatan prosedur berhasil");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-prosedur.create", ["id" => Crypt::encrypt($idPelayanan)])
                ->with("error", "Terjadi kesalahan dalam proses data");
        }
    }
}
