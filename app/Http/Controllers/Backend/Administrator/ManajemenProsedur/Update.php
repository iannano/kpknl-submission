<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenProsedur;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\ProsedurPelayanan;
use App\Http\Repository\Implement\PelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $prosedurModel;
    protected $prosedurRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->prosedurModel = new ProsedurPelayanan();
        $this->prosedurRepository = new ProsedurPelayananRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        //Get id in url parameter
        $idProsedur = Crypt::decrypt($request->id);

        $data["dataProsedur"] = $this->prosedurRepository->findById(NULL, NULL, $idProsedur);
        $data["dataPelayanan"] = $this->pelayananRepository->findById(NULL, NULL, $data["dataProsedur"]["id_pelayanan"]);

        return view("backend.administrator.manajemen-prosedur.update", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idProsedur = Crypt::decrypt($request->post("id_prosedur"));
        $idPelayanan = Crypt::decrypt($request->post("id_pelayanan"));
        $prosedur = $request->post("prosedur");
        $tahapan = $request->post("tahapan");

        //Filled model with data
        $prosedurModel = $this->prosedurModel->find($idProsedur);
        $prosedurModel->prosedur = $prosedur;
        $prosedurModel->tahapan = $tahapan;

        //Update data
        $result = $this->prosedurRepository->save($prosedurModel, $idProsedur);

        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-prosedur.index", ["id"=>Crypt::encrypt($idPelayanan)])
                ->with("information","Update prosedur sukses");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-prosedur.update", ["id"=>Crypt::encrypt($idProsedur)])
                ->with("error", "Update prosedur gagal");
        }
    }
}
