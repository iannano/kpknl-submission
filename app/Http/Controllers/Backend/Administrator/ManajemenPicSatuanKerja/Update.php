<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPicSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\PicSatuanKerja;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\PicSatuanKerjaRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $picSatuanKerjaModel;
    protected $picSatuanKerjaRepository;
    protected $seksiRepository;
    protected $pegawaiRepository;

    public function __construct()
    {
        $this->picSatuanKerjaModel = new PicSatuanKerja();
        $this->picSatuanKerjaRepository = new PicSatuanKerjaRepository();
        $this->seksiRepository = new SeksiRepository();
        $this->pegawaiRepository = new PegawaiRepository();
    }

    public function view(Request $request)
    {
        $idPicSatuanKerja = Crypt::decrypt($request->get("id"));
        $data["listSeksi"] = $this->seksiRepository->findAll();
        $data["dataPicSatuanKerja"] = $this->picSatuanKerjaRepository->findById(
            NULL,
            NULL,
            ["id"=>$idPicSatuanKerja]
        );

        $data["listPegawai"] = $this->pegawaiRepository->findByParam(
            NULL,
            ["id_seksi"=>$data["dataPicSatuanKerja"]["id_seksi"]]
        );
        return view("backend.administrator.manajemen-pic-satuan-kerja.update", $data);
    }

    public function submit(Request $request)
    {
        $idPicSatuanKerja = Crypt::decrypt($request->post("id-pic-satuan-kerja"));
        $idSatuanKerja = Crypt::decrypt($request->post('id-satuan-kerja'));
        $idSeksi = $request->post('id-seksi');
        $idPegawai = $request->post('id-pegawai');

        $picSatuanKerjaModel = $this->picSatuanKerjaModel->find($idPicSatuanKerja);
        $picSatuanKerjaModel->id_seksi = $idSeksi;
        $picSatuanKerjaModel->id_pegawai = $idPegawai;

        $result = $this->picSatuanKerjaRepository->save($picSatuanKerjaModel, $idPicSatuanKerja);
        if($result != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.index",["id"=>Crypt::encrypt($idSatuanKerja)])
                ->with("information","Perubahan Data PIC Satuan Kerja Berhasil");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.update",["id"=>Crypt::encrypt($idPicSatuanKerja)])
                ->with("error","Terjadi Kesalahan Dalam Perubahan Data PIC Satuan Kerja");
        }
    }

}
