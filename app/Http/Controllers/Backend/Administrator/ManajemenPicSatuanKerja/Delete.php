<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPicSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\PicSatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $picSatuanKerjaRepository;

    public function __construct()
    {
        $this->picSatuanKerjaRepository = new PicSatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        $idPicSatuanKerja = Crypt::decrypt($request->get("id"));
        $data["dataPicSatuanKerja"] = $this->picSatuanKerjaRepository->findById(
            NULL,
            ["seksi","pegawai","satuankerja"],
            ["id"=>$idPicSatuanKerja]
        );
        return view("backend.administrator.manajemen-pic-satuan-kerja.delete", $data);
    }

    public function submit(Request $request)
    {
        $idPicSatuanKerja = Crypt::decrypt($request->post("id-pic-satuan-kerja"));
        $idSatuanKerja = Crypt::decrypt($request->post("id-satuan-kerja"));
        $request = $this->picSatuanKerjaRepository->delete($idPicSatuanKerja);
        if($request != "")
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.index",["id"=>Crypt::encrypt($idSatuanKerja)])
                ->with("information","Data PIC Satuan Kerja Berhasil Dihapus");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.delete",["id"=>Crypt::encrypt($idPicSatuanKerja)])
                ->with("error","Terjadi Kesalahan Dalam Menghapus Data PIC Satuan Kerja");
        }
    }

}
