<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPicSatuanKerja;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\PicSatuanKerjaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Index extends Controller implements ViewController
{
    protected $picSatuanKerjaRepository;

    public function __construct()
    {
        $this->picSatuanKerjaRepository = new PicSatuanKerjaRepository();
    }

    public function view(Request $request)
    {
        $idSatuanKerja = Crypt::decrypt($request->get('id'));
        $data['idSatuanKerja'] = $idSatuanKerja;
        $data["listPicSatuanKerja"] = $this->picSatuanKerjaRepository->findByParam(
            ["*"],
            ['id_satuan_kerja'=>$idSatuanKerja],
            ["seksi","pegawai"]
        );

        return view("backend.administrator.manajemen-pic-satuan-kerja.index", $data);
    }

}
