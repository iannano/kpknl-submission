<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenPicSatuanKerja;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\PicSatuanKerja;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\PicSatuanKerjaRepository;
use App\Http\Repository\Implement\SeksiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Create extends Controller implements FormController
{
    protected $picSatuanKerjaModel;
    protected $picSatuanKerjaRepository;
    protected $pegawaiRepository;
    protected $seksiRepository;

    public function __construct()
    {
        $this->picSatuanKerjaModel = new PicSatuanKerja();
        $this->picSatuanKerjaRepository = new PicSatuanKerjaRepository();
        $this->pegawaiRepository = new PegawaiRepository();
        $this->seksiRepository = new SeksiRepository();
    }

    public function view(Request $request)
    {
        $data['idSatuanKerja'] = Crypt::decrypt($request->get('id'));
        $data['listSeksi'] = $this->seksiRepository->findAll();

        return view('backend.administrator.manajemen-pic-satuan-kerja.create', $data);
    }

    public function submit(Request $request)
    {
        $idSatuanKerja = Crypt::decrypt($request->post("id-satuan-kerja"));
        $idSeksi = $request->post("id-seksi");
        $idPegawai = $request->post("id-pegawai");

        $this->picSatuanKerjaModel->id_satuan_kerja = $idSatuanKerja;
        $this->picSatuanKerjaModel->id_seksi = $idSeksi;
        $this->picSatuanKerjaModel->id_pegawai = $idPegawai;

        $result = $this->picSatuanKerjaRepository->save($this->picSatuanKerjaModel);
        if($result != "")
        {
            //Success
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.index",["id"=>Crypt::encrypt($idSatuanKerja)])
                ->with("information","Penentuan PIC Satuan Kerja Berhasil");
        }
        else
        {
            //Failed
            return redirect()
                ->route("internal-kpknl.manajemen-pic-satuan-kerja.create",["id"=>Crypt::encrypt($idSatuanKerja)])
                ->with("error","Terjadi Kesalahan Dalam Penentuan PIC Satuan Kerja");

        }
    }

}
