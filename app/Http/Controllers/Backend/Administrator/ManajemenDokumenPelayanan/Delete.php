<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenDokumenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Delete extends Controller implements FormController
{
    protected $dokumenRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->dokumenRepository = new DokumenRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        //Get id in url param
        $id = Crypt::decrypt($request->get("id"));

        //Get data dokumen
        $data["dataDokumen"] = $this->dokumenRepository->findById(NULL, NULL, $id);
        $data["dataPelayanan"] = $this->pelayananRepository->findByParam(NULL, ["id"=> $data["dataDokumen"]["id_pelayanan"]]);

        return view("backend.administrator.manajemen-dokumen.delete", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPelayanan = Crypt::decrypt($request->post("idPelayanan"));
        $idDokumen = Crypt::decrypt($request->post("idDokumen"));

        //Delete data from database
        $result = $this->dokumenRepository->delete($idDokumen);

        //Define query result
        if($result != "")
        {
            //Success, back to manajamen dokumen index and show information
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.index", ["id"=>Crypt::encrypt($idPelayanan)])
                ->with("information","Hapus data berhasil");
        }
        else
        {
            //Failed, back to delete form and show information
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.delete",["id"=>Crypt::encrypt($idDokumen)])
                ->with("error","Terjadi kesalahan dalam menghapus data");
        }
    }
}
