<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenDokumenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Dokumen;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Update extends Controller implements FormController
{
    protected $dokumenModel;
    protected $dokumenRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->dokumenModel     = new Dokumen();
        $this->dokumenRepository = new DokumenRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        $id = Crypt::decrypt($request->get("id"));
        $data["dataDokumen"] = $this->dokumenRepository->findById(NULL, NULL, $id);
        $data["dataPelayanan"] = $this->pelayananRepository->findByParam(NULL, ["id"=>$data["dataDokumen"]["id_pelayanan"]]);
        return view("backend.administrator.manajemen-dokumen.update", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPelayanan    = Crypt::decrypt($request->post("idPelayanan"));
        $idDokumen      = Crypt::decrypt($request->post("idDokumen"));
        $dokumen        = $request->post("dokumen");
        $keterangan     = $request->post("keterangan");

        //Filled to model
        $dokumenModel = $this->dokumenModel->find($idDokumen);
        $dokumenModel->dokumen = $dokumen;
        $dokumenModel->keterangan = $keterangan;

        //Execute update data
        $result = $this->dokumenRepository->save($dokumenModel, $idDokumen);
        if($result != "")
        {
            //Success
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.index", ["id"=>Crypt::encrypt($idPelayanan)])
                ->with("information", "Update Data Berhasil");
        }
        else
        {
            //Failed
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.update",["id"=>Crypt::encrypt($idDokumen)])
                ->with("error", "Terjadi Kesalahan Dalam Update Data");
        }
    }
}
