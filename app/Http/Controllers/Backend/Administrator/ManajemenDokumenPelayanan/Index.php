<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenDokumenPelayanan;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Index extends Controller implements ViewController
{
    protected $dokumenRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->dokumenRepository    = new DokumenRepository();
        $this->pelayananRepository  = new PelayananRepository();
    }

    public function view(Request $request)
    {
        //Get id in url parameter
        $idPelayanan = Crypt::decrypt($request->get("id"));

        //Get data pelayanan base by id pelayanan
        $data['dataPelayanan'] = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);

        //Get list dokumen base by id pelayanan
        $data['listDokumen'] = $this->dokumenRepository->findByParam(
            NULL,
            ["id_pelayanan"=>$idPelayanan]
        );

        return view("backend.administrator.manajemen-dokumen.index", $data);
    }

}
