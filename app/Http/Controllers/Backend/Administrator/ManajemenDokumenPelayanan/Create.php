<?php

namespace App\Http\Controllers\Backend\Administrator\ManajemenDokumenPelayanan;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\Dokumen;
use App\Http\Repository\Implement\DokumenRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class Create extends Controller implements FormController
{
    protected $dokumenModel;
    protected $dokumenRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->dokumenModel         = new Dokumen();
        $this->dokumenRepository    = new DokumenRepository();
        $this->pelayananRepository  = new PelayananRepository();
    }
    public function view(Request $request)
    {
        //Get id in url parameter
        $idPelayanan = Crypt::decrypt($request->id);

        //Get data pelayanan base by id
        $data['dataPelayanan'] = $this->pelayananRepository->findById(NULL, NULL, $idPelayanan);

        //Return view
        return view("backend.administrator.manajemen-dokumen.create", $data);
    }

    public function submit(Request $request)
    {
        //Get post data
        $idPelayanan    = Crypt::decrypt($request->post("idPelayanan"));
        $dokumen        = $request->post("dokumen");
        $keterangan     = $request->post("keterangan");

        //Fill model with post data
        $this->dokumenModel->id_pelayanan   = $idPelayanan;
        $this->dokumenModel->dokumen        = $dokumen;
        $this->dokumenModel->keterangan     = $keterangan;

        //Execute save query
        $result = $this->dokumenRepository->save($this->dokumenModel);

        //Define query result
        if($result != "")
        {
            //Success, return to index manajemen dokumen and show information
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.index",["id"=>Crypt::encrypt($idPelayanan)])
                ->with("information","");
        }
        else
        {
            //Failed, return ro create form and show information
            return redirect()
                ->route("internal-kpknl.manajemen-dokumen.create")
                ->with("error","Terjadi kesalahan dalam penyimpanan data");
        }
    }

}
