<?php

namespace App\Http\Controllers\Backend\Lelang\PermohonanMasuk;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class Index extends Controller implements ViewController
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findDetailPelayananBySeksi(3);
        return view("backend.lelang.permohonan-masuk.index", $data);
    }
}
