<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Factory\ViewController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Logout extends Controller implements ViewController
{
    public function __construct()
    {
    }

    public function view(Request $request)
    {

        $request->session()->forget("access-internal");
        return redirect()
            ->route("internal-kpknl.auth.login");
    }
}
