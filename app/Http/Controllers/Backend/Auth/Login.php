<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Factory\FormController;
use App\Http\Middleware\app\CheckAuthInternalKPKNL;
use App\Http\Repository\Implement\PegawaiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

/**
 * Class Login
 * @package App\Http\Controllers\Backend\Auth
 */
class Login extends Controller implements FormController
{
    protected $pegawaiRepository;
    /**
     * Login constructor.
     */
    public function __construct()
    {
        $this->pegawaiRepository = new PegawaiRepository();
    }

    /**
     * Use for show auth form internal kpknl
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function view(Request $request)
    {
        if(!$request->session()->get('access-internal'))
        {
            return view("backend.auth.login");
        }
        else
        {
            return redirect()
                ->route("internal-kpknl.dashboard");
        }
    }

    /**
     * Use for processing data from auth form internal kpknl
     * @param Request $request
     * @return mixed|void
     */
    public function submit(Request $request)
    {
        //Get post data
        $username   = $request->post("username");
        $password   = $request->post("password");

        //Get by parameter username and password
        $dataPegawai = $this->pegawaiRepository->findByParam(
            ["id","nama","role","id_seksi"],
            ["username"=>$username,"password"=>sha1(md5($password))]
        );

        //Validate result
        if(count($dataPegawai) > 0)
        {
            $data = array(
                "id"    => $dataPegawai[0]->id,
                "nama"  => $dataPegawai[0]->nama,
                "role"  => $dataPegawai[0]->role,
                "seksi" => $dataPegawai[0]->id_seksi
            );
            $request->session()->put("access-internal",$data);
            return redirect()
                ->route("internal-kpknl.dashboard");
        }
        else
        {
            //User not exist
            return redirect()
                ->route("internal-kpknl.auth.login")
                ->with("error","User pegawai tidak ditemukan");
        }
    }

}
