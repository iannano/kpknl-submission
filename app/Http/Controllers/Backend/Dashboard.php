<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Factory\ViewController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Dashboard extends Controller implements ViewController
{
    public function __construct()
    {
    }

    public function view(Request $request)
    {
        return view("backend.dashboard.index");
    }
}
