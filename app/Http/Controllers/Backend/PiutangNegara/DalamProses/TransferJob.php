<?php

namespace App\Http\Controllers\Backend\PiutangNegara\DalamProses;

use App\Http\Controllers\Factory\FormController;
use App\Http\Controllers\Webhook\Telegram\NotificationTimPenilai;
use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\PelayananRepository;
use App\Mail\InformasiTimPenilai;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class TransferJob extends Controller implements FormController
{
    protected $detailPelayananModel;
    protected $detailPelayananRepository;
    protected $pelayananRepository;
    protected $notificationTimPenilai;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->pelayananRepository = new PelayananRepository();
        $this->notificationTimPenilai = new NotificationTimPenilai();
    }

    public function view(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->get("id"));
        $data["dataDetailPelayanan"] = $this->detailPelayananRepository->findById(
            NULL,
            ["satuankerja","pelayanan"],
            ["id"=>$idDetailPelayanan]
        );
        $data['listPelayananPenilai'] = $this->pelayananRepository->findByParam(
            NULL,
            ["id_seksi"=>3]
        );
        return view("backend.piutang-negara.dalam-proses.transfer-job", $data);
    }

    public function submit(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->get("id"));

        //Create detail pelayanan penilaian
        $this->createPelayananPenilaian($request);

        //Update status detail pelayanan PKN
        $this->detailPelayananModel = $this->detailPelayananModel->find($idDetailPelayanan);
        $this->detailPelayananModel->waktu_berakhir = NULL;
        $this->detailPelayananModel->status = 2;
        $this->detailPelayananRepository->save($this->detailPelayananModel, $idDetailPelayanan);

        return redirect()
            ->route("internal-kpknl.piutang-negara.dalam-proses.index")
            ->with('information',"Transfer Pelayanan Berhasil");
    }

    private function createPelayananPenilaian(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->post("id"));

        //Data pelayanan dari PKN
        $dataDetailPelayanan = $this->detailPelayananRepository->findByParam(
            NULL,
            ["id"=>$idDetailPelayanan],
            ["pelayanan"]
        );

        $idPelayanan = $request->post('id-pelayanan');
        $this->detailPelayananModel->id_pelayanan = $idPelayanan;
        $this->detailPelayananModel->id_satuan_kerja = $dataDetailPelayanan[0]['id_satuan_kerja'];
        $this->detailPelayananModel->tiket = $dataDetailPelayanan[0]['tiket'];
        $this->detailPelayananModel->waktu_berakhir = Carbon::now()->addDays($dataDetailPelayanan[0]['pelayanan']['waktu_sop']);
        $this->detailPelayananModel->id_detail_pelayanan = $idDetailPelayanan;
        $this->detailPelayananModel->status = 0;

        $this->detailPelayananRepository->save($this->detailPelayananModel);

        $this->sentEmail($idDetailPelayanan);
        $this->notificationTimPenilai->sent($idDetailPelayanan);
    }

    private function sentEmail($idDetailPelayanan)
    {
        $data['dataDetailPelayanan'] = $this->detailPelayananRepository->findById(
            NULL,
            ["pelayanan","satuankerja"],
            ["id"=>$idDetailPelayanan]
        );
        Mail::to($data['dataDetailPelayanan']['satuankerja']["email"])
            ->send(new InformasiTimPenilai($data));
    }
}
