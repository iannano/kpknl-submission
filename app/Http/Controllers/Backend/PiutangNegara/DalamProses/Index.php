<?php

namespace App\Http\Controllers\Backend\PiutangNegara\DalamProses;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class Index extends Controller implements ViewController
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $idPegawai = $request->session()->get("access-internal")['id'];
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findByParam(
            NULL,
            ["id_pegawai"=>$idPegawai],
            ["satuankerja","pelayanan"],
            "id",
            "DESC"
        );
        return view("backend.piutang-negara.dalam-proses.index", $data);
    }
}
