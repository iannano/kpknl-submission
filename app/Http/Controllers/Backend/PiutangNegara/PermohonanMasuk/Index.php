<?php

namespace App\Http\Controllers\Backend\PiutangNegara\PermohonanMasuk;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class Index extends Controller implements ViewController
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findDetailPelayananBySeksi(4);
        return view("backend.piutang-negara.permohonan-masuk.index", $data);
    }
}
