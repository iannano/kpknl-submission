<?php

namespace App\Http\Controllers\Backend\PiutangNegara\PermohonanMasuk;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\PelayananRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;

class AssignJob extends Controller implements FormController
{
    protected $detailPelayananModel;
    protected $detailPelayananRepository;
    protected $pegawaiRepository;
    protected $pelayananRepository;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->pegawaiRepository = new PegawaiRepository();
        $this->pelayananRepository = new PelayananRepository();
    }

    public function view(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->get("id"));
        $data["dataDetailPelayanan"] = $this->detailPelayananRepository->findById(
            NULL,
            ["satuankerja","pelayanan"],
            ["id"=>$idDetailPelayanan]
        );
        return view("backend.piutang-negara.permohonan-masuk.assign-job", $data);
    }

    public function submit(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->post("id"));
        $idPegawai = $request->session()->get("access-internal")["id"];
        $this->detailPelayananModel = $this->detailPelayananModel->find($idDetailPelayanan);

        $dataPelayanan = $this->pelayananRepository->findById(
            NULL,
            NULL,
            ['id'=>$this->detailPelayananModel->id_pelayanan]
        );

        if($this->detailPelayananModel->id_pegawai !== 0)
        {
            $this->detailPelayananModel->waktu_berakhir = Carbon::now()->addDays($dataPelayanan['waktu_sop']);
            $this->detailPelayananModel->waktu_mulai = Carbon::now();
        }
        $this->detailPelayananModel->id_pegawai = $idPegawai;
        $this->detailPelayananRepository->save($this->detailPelayananModel, $idDetailPelayanan);
        return redirect()
            ->route("internal-kpknl.piutang-negara.permohonan-masuk.index")
            ->with("information","Assignment Tugas Berhasil");
    }

}
