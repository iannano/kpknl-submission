<?php

namespace App\Http\Controllers\Backend\Penilaian\DalamProses;

use App\Http\Controllers\Factory\FormController;
use App\Http\Models\DetailPelayanan;
use App\Http\Models\DetailProsedurPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\DetailProsedurPelayananRepository;
use App\Http\Repository\Implement\ProsedurPelayananRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class CurrentStep extends Controller implements FormController
{
    protected $detailPelayananModel;
    protected $detailProsedurPelayananModel;
    protected $detailPelayananRepository;
    protected $prosedurPelayananRepository;
    protected $detailProsedurPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->detailProsedurPelayananModel = new DetailProsedurPelayanan();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->prosedurPelayananRepository = new ProsedurPelayananRepository();
        $this->detailProsedurPelayananRepository = new DetailProsedurPelayananRepository();
    }

    public function view(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->get("id"));
        $data["dataDetailPelayanan"] = $this->detailPelayananRepository->findById(
            NULL,
            ["satuankerja","pelayanan"],
            ["id"=>$idDetailPelayanan]
        );
        $data['listProsedurPelayanan'] = $this->prosedurPelayananRepository->findByParam(
            NULL,
            ["id_pelayanan"=>$data['dataDetailPelayanan']['id_pelayanan']]
        );
        $listDetailProsedurPelayanan = $this->detailProsedurPelayananRepository->findByParam(
            NULL,
            ["id_detail_pelayanan"=>$data['dataDetailPelayanan']['id']]
        );

        $data['arrayIdDetailProsedurPelayanan'] = $this->makeArrayIdDetailProsedurPelayanan($listDetailProsedurPelayanan);

        return view("backend.penilaian.dalam-proses.current-step", $data);
    }

    public function submit(Request $request)
    {
        $arraySubmit = array();
        $idDetailPelayanan = Crypt::decrypt($request->post("id"));

        //Update status pelayanan to di proses
        if($request->post("prosedur"))
        {
            $this->detailPelayananModel = $this->detailPelayananModel->find($idDetailPelayanan);
            $this->detailPelayananModel->status = 1;
            $this->detailPelayananRepository->save($this->detailPelayananModel, $idDetailPelayanan);

            foreach ($request->post('prosedur') as $key=>$value)
            {
                $arraySubmit[] = $value;
            }
        }

        $listDetailProsedurPelayanan = $this->detailProsedurPelayananRepository->findByParam(
            NULL,
            ["id_detail_pelayanan"=>$idDetailPelayanan]
        );

        $arrayListDetailProsudurPelayanan = $this->makeArrayIdDetailProsedurPelayanan($listDetailProsedurPelayanan);

        $addedData = array_diff($arraySubmit, $arrayListDetailProsudurPelayanan);
        $deletedData = array_diff($arrayListDetailProsudurPelayanan, $arraySubmit);

        //Add and Delete prosedur
        $this->addDetailProsedurPelayanan($addedData, $idDetailPelayanan);
        $this->deleteDetailProsedurPelayanan($deletedData, $idDetailPelayanan);

        //Send notification to telegram
        $this->notificationProgress->send($idDetailPelayanan);

        return redirect()
            ->route("internal-kpknl.penilaian.dalam-proses.current-step",["id"=>Crypt::encrypt($idDetailPelayanan)])
            ->with("information","Data Berhasil Diupdate");
    }

    private function makeArrayIdDetailProsedurPelayanan($listDetailProsedurPelayanan)
    {
        $dataArray = array();
        foreach ($listDetailProsedurPelayanan as $detailProsedurPelayanan)
        {
            $dataArray[] = $detailProsedurPelayanan->id_prosedur_pelayanan;
        }

        return $dataArray;
    }

    private function addDetailProsedurPelayanan($addedArray, $idDetailPelayanan)
    {
        foreach ($addedArray as $key=>$value)
        {
            $this->detailProsedurPelayananModel = new DetailProsedurPelayanan();
            $this->detailProsedurPelayananModel->id_detail_pelayanan = $idDetailPelayanan;
            $this->detailProsedurPelayananModel->id_prosedur_pelayanan = $value;
            $this->detailProsedurPelayananModel->waktu = Carbon::now();
            $this->detailProsedurPelayananRepository->save($this->detailProsedurPelayananModel);
        }
    }

    private function deleteDetailProsedurPelayanan($deletedData, $idDetailPelayanan)
    {
        foreach ($deletedData as $key=>$value)
        {
            $this->detailProsedurPelayananRepository
                ->deleteByIdDetailPelayananAndIdProsedurPelayanan($idDetailPelayanan, $value);
        }
    }
}
