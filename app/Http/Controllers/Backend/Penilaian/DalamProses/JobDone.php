<?php

namespace App\Http\Controllers\Backend\Penilaian\DalamProses;

use App\Http\Controllers\Factory\EndpointController;
use App\Http\Controllers\Webhook\Telegram\NotificationJobDone;
use App\Http\Models\DetailPelayanan;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Mail\PelayananSelesai;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class JobDone extends Controller implements EndpointController
{
    protected $detailPelayananModel;
    protected $detailPelayananRepository;
    protected $notificationJobDone;

    public function __construct()
    {
        $this->detailPelayananModel = new DetailPelayanan();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->notificationJobDone = new NotificationJobDone();
    }

    public function submit(Request $request)
    {
        $idDetailPelayanan = Crypt::decrypt($request->get("id"));
        $this->detailPelayananModel = $this->detailPelayananModel->find($idDetailPelayanan);
        $this->detailPelayananModel->waktu_selesai = Carbon::now();
        $this->detailPelayananModel->status = 3;
        $this->detailPelayananRepository->save($this->detailPelayananModel, $idDetailPelayanan);

        $data['dataDetailPelayanan'] = $this->detailPelayananRepository->findById(
            NULL,
            ["pelayanan","satuankerja"],
            ["id"=>$idDetailPelayanan]
        );

        if($data['dataDetailPelayanan']['id_detail_pelayanan'] === NULL)
        {
            $this->notificationJobDone->send($idDetailPelayanan);

            Mail::to($data['dataDetailPelayanan']['satuankerja']["email"])
                ->send(new PelayananSelesai($data));

            $parentPelayanan = $this->detailPelayananRepository->findById(
                NULL,
                ["pelayanan","satuankerja"],
                ["id"=>$data['dataDetailPelayanan']['id_detail_pelayanan']]
            );
            $this->detailPelayananModel = $this->detailPelayananModel->find($data['dataDetailPelayanan']['id_detail_pelayanan']);
            $this->detailPelayananModel->status = 1;
            $this->detailPelayananModel->waktu_berakhir = Carbon::now()->addDays($parentPelayanan['pelayanan']['waktu_sop']);
            $this->detailPelayananRepository->save($this->detailPelayananModel, $data['dataDetailPelayanan']['id_detail_pelayanan']);
        }

        return redirect()
            ->route("internal-kpknl.penilaian.dalam-proses.index")
            ->with("information","Tugas Selesai");
    }

}
