<?php

namespace App\Http\Controllers\Backend\Penilaian\PermohonanMasuk;

use App\Http\Controllers\Factory\ViewController;
use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller implements ViewController
{
    protected $detailPelayananRepository;

    public function __construct()
    {
        $this->detailPelayananRepository = new DetailPelayananRepository();
    }

    public function view(Request $request)
    {
        $data['listDetailPelayanan'] = $this->detailPelayananRepository->findDetailPelayananBySeksi(3);
        return view("backend.penilaian.permohonan-masuk.index", $data);
    }
}
