<?php

namespace App\Mail;

use App\Http\Repository\Implement\DetailPelayananRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TiketPelayanan extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataEmail)
    {
        $this->data['data'] = $dataEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("kpknlbiak@kemenkeu.go.id")
            ->subject("Tiket Pelayanan KPKNL Biak")
            ->view("email.tiket-pelayanan", $this->data);
    }
}
