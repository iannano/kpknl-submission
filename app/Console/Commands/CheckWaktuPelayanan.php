<?php

namespace App\Console\Commands;

use App\Http\Models\PollWarning;
use App\Http\Repository\Implement\DetailPelayananRepository;
use App\Http\Repository\Implement\PegawaiRepository;
use App\Http\Repository\Implement\PollWarningRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class CheckWaktuPelayanan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:checkwaktupelayanan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron job untuk melakukan pengecekan waktu pelayanan';

    protected $pollWarningModel;
    protected $detailPelayananRepository;
    protected $pollWarningRepository;
    protected $pegawaiRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pollWarningModel = new PollWarning();
        $this->detailPelayananRepository = new DetailPelayananRepository();
        $this->pollWarningRepository = new PollWarningRepository();
        $this->pegawaiRepository = new PegawaiRepository();
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        //Require parameter
        $parameterGetTelegramKepalaKantor = ["role"=>"kepala_kantor"];

        $listPollWarning = $this->detailPelayananRepository->pollForWarning();
        $increment = 1;
        $response = "Pelayanan yang mendekati batas akhir dan belum terselesaikan sebagai berikut: \n";
        foreach ($listPollWarning as $pollWarning)
        {
            $posibilityTimeToFinish = new Carbon($pollWarning->waktu_berakhir);
            $currentTime = Carbon::now();
            $waktuToleransi = $pollWarning->waktu_sop - 3;
            if($posibilityTimeToFinish->diff($currentTime)->days <= $waktuToleransi)
            {
                $response .= "<pre>".$increment.". PIC KPKNL: ".$pollWarning->nama."\n ";
                $response .= "Satuan Kerja: ".$pollWarning->satuan_kerja." \n";
                $response .= "Jenis Pelayanan: ".$pollWarning->pelayanan." \n";
                $response .= "Tanggal Berakhir: ".$pollWarning->waktu_berakhir."</pre>";
                $increment++;
            }
        }

        $dataKepalaKantor = $this->pegawaiRepository->findByParam(NULL, $parameterGetTelegramKepalaKantor);

        Telegram::sendMessage([
            "chat_id"       => $dataKepalaKantor[0]['chat_id'],
            "text"          => $response,
            "parse_mode"    => "html"
        ]);
    }
}
