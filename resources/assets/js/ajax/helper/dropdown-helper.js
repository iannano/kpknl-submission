"use strict";

class DropdownHelper{
    constructor()
    {

    }

    static resetOption(selectDOM, optionText = null)
    {
        //Remove all option
        selectDOM.options.length = 0;
        if(optionText !== null)
        {
            //Create DOM option
            let option = document.createElement("option");
            option.value = "";
            option.text = "--- "+optionText+" ---";
            selectDOM.appendChild(option);

            //Set default option
            selectDOM.options[0].selected;
        }
    }
}

export default DropdownHelper;
