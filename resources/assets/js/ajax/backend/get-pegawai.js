"use script";
import axios from "axios";
import DropdownHelper from "../helper/dropdown-helper";

let seksiDOM = document.getElementById("seksi");
let pegawaiDOM  = document.getElementById("pegawai");

//Async request
let asyncRequest = (FormData) =>{
    return axios({
        method: 'POST',
        url: baseUrl+'/ajax/pegawai/getBySeksi',
        dataType: "JSON",
        xsrfCookieName: 'XSRF-TOKEN',
        xsrfHeaderName: 'X-XSRF-TOKEN',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        data: FormData
    });
};

//Check DOM
if(!!seksiDOM)
{
    //Dom manipulation
    seksiDOM.onchange = () =>{
        //Remove all pegawai option value
        DropdownHelper.resetOption(pegawaiDOM,"Pilih Pegawai");

        //Create form data
        let formData = new FormData();

        //Append value to form data
        formData.append("idSeksi", seksiDOM.value);

        //Execute async request
        asyncRequest(formData)
            .then((response)=>{
                //Remove disable attribute
                pegawaiDOM.removeAttribute("disabled");

                //Request result
                let result = response.data;

                //Check result data length
                if(result.length !== 0)
                {
                    result.forEach((data)=>{
                        //Creating DOM
                        let option = document.createElement("option");
                        option.value = data.id;
                        option.text = data.nama;

                        //Append select
                        pegawaiDOM.appendChild(option);
                    });
                }

            })
            .catch((reject)=>{
                console.error(reject);
            });
    }
}
