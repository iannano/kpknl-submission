"use strict";

let currentStepDom = document.getElementsByClassName("current-step");
let countCurrentStepDom = currentStepDom.length;

let giveCheckedAttribute = (index) =>{
    for(let i = 0; i < index; i++)
    {
        currentStepDom[i].checked = true;
    }
};

let removeCheckedAttribute = (index) =>{
    for (let i = (countCurrentStepDom-1); i > index; i--)
    {
        currentStepDom[i].checked = false;
    }
};

if(!!currentStepDom)
{
    for (let i = 0; i < countCurrentStepDom; i++)
    {
        currentStepDom[i].onclick = () =>{
            removeCheckedAttribute(i);
            giveCheckedAttribute(i);
        };
    }
}
