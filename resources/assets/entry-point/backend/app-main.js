global.baseUrl = "http://127.0.0.1:8000";

//CSS
require ("../../plugins/bootstrap-3.4.1/css/bootstrap.css");
require ("../../plugins/AdminLTE-2.4.15/dist/css/AdminLTE.css");
require ("../../plugins/AdminLTE-2.4.15/dist/css/skins/skin-blue-light.css");
require ("../../css/custom-backend.css");
require ("datatables.net-bs/css/dataTables.bootstrap.css");

//JS
require ("jquery");
require ("../../plugins/bootstrap-3.4.1/js/bootstrap");
require ("../../plugins/AdminLTE-2.4.15/dist/js/adminlte");
require ("../../js/init-ckeditor");
require ("datatables.net-bs/js/dataTables.bootstrap");
require ("datatables.net-fixedheader-bs");
require ("datatables.net-responsive-bs");

//Ajax
require ("../../js/ajax/backend/get-pegawai");

//Datatables
require ("../../js/datatables/pegawai");
require ("../../js/datatables/satuan-kerja");
require ("../../js/datatables/pelayanan");
require ("../../js/datatables/permohonan-masuk");
require ("../../js/datatables/dalam-proses");

//DOM Manipulation
require ("../../js/helper/checkbox-current-step");
