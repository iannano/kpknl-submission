<html>
<head>
    <title>KPKNL Biak</title>
    <!-- Block Meta Web -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Block CSS -->
    
    <link href="{{ URL::asset('assets/core.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('assets/backend-main.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="#" class="logo">
            <center>
                <span class="logo-mini"><b>DK</b></span>
                <span class="logo-lg">Dashboard KPKNL</span>
            </center>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="user user-menu">
                        <a href="{{ route('internal-kpknl.auth.logout') }}">Sign out</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                @include("backend.menu.main-menu")
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        @yield("content")
    </div>
</div>
</body>
<footer>
    <script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    
    <script src="{{ URL::asset('assets/core.js') }}" type="text/javascript"></script>
    
    <script src="{{ URL::asset('assets/backend-main.js') }}" type="text/javascript"></script>
    
</footer>
</html>
