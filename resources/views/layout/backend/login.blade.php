<html>
<head>
    <title>KPKNL Biak - Internal Auth</title>
    <!-- Block Meta Web -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Block CSS -->
    
        <link href="{{ URL::asset('assets/core.css') }}" rel="stylesheet">
    
        <link href="{{ URL::asset('assets/backend-main.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="hold-transition login-page">
    @yield("content")
</body>
<footer>
    <script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    
        <script src="{{ URL::asset('assets/core.js') }}" type="text/javascript"></script>
    
        <script src="{{ URL::asset('assets/backend-main.js') }}" type="text/javascript"></script>
    
</footer>
</html>
