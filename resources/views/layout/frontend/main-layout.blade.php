<html>
<head>
    <title>KPKNL Biak</title>
    <!-- Block Meta Web -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Block CSS -->
    
        <link href="{{ URL::asset('assets/core.css') }}" rel="stylesheet">
    
        <link href="{{ URL::asset('assets/frontend-main.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
@include("frontend.menu.main-menu")
@yield('content')
</body>
<footer class="footer-section">
    <div class="container">
        <div class="col-md-4 hidden-xs">
            <span>
                <img class="img-responsive" src="{{url('assets/images/logo-desktop-kemenkeu.png')}}"/>
                <p class="address">
                    Hak Cipta Kementerian Keuangan Republik Indonesia
                Manajemen Situs Kemenkeu - Gedung Djuanda I Lt. 9
                Jl. Dr.Wahidin Raya No 1 Jakarta 10710
                Telp: 134 Fax: 021-3500842
                </p>
            </span>
        </div>
    </div>
    <script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    
        <script src="{{ URL::asset('assets/core.js') }}" type="text/javascript"></script>
    
        <script src="{{ URL::asset('assets/frontend-main.js') }}" type="text/javascript"></script>
    
</footer>
</html>
