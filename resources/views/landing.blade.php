@extends("layout.frontend.main-layout")
@section("content")
<section>
    <div class="container-fluid main-container content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">"Test"</h2>
                </div>
                <div class="col-md-4 text-center">
                    <h4><b>Knowledge Base</b></h4>
                    <a href="#">
                        <img class="img-responsive" src="{{url("assets/images/1.png")}}"/>
                    </a>
                </div>
                <div class="col-md-4 text-center">
                    <h4><b>Buat Tiket</b></h4>
                    <a href="#">
                        <img class="img-responsive" src="{{url("assets/images/2.png")}}"/>
                    </a>
                </div>
                <div class="col-md-4 text-center">
                    <h4><b>Cari Tiket</b></h4>
                    <a href="#">
                        <img class="img-responsive" src="{{url("assets/images/3.png")}}"/>
                    </a>
                </div>
            </div>
            <div class="row padding-top-5em">
                <div class="col-md-4">
                    <img class="img-responsive" src="{{url("assets/images/call-center.png")}}"/>
                </div>
                <div class="col-md-8">
                    <h4>Kunjungi Kami:</h4>
                    <h5>Direktorat Jenderal Kekayaan Negara, Kementerian Keuangan Republik Indonesia
                        Gedung Syafruddin Prawiranegara II Lantai 7 Jalan Lapangan Banteng Timur 2-4,
                        Jakarta Pusat, 10710</h5>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15955.918529391061!2d136.0911578!3d-1.1748134!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xec82ef8973d50b37!2sKPKNL+Biak!5e0!3m2!1sen!2sid!4v1565180247050!5m2!1sen!2sid" width="100%" height="50%" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
@endsection
