@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h3>Manajemen Pelayanan <small>KPKNL Biak</small></h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li>Create</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Buat Pelayanan Baru</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
                </div>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <form action="{{ route('internal-kpknl.manajemen-pelayanan.create.submit') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Seksi PIC</label>
                        <select name="seksi" class="form-control">
                            <option value="">-- Pilih Seksi PIC --</option>
                            @foreach($listSeksi as $seksi)
                                <option value="{{ $seksi->id }}">{{ $seksi->seksi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pelayanan <small class="text-danger">*</small></label>
                        <input type="text" name="pelayanan" class="form-control" placeholder="Nama Pelayanan"/>
                    </div>
                    <div class="form-group">
                        <label>Waktu SOP <small class="text-danger">*</small></label>
                        <div class="input-group">
                            <input type="number" name="waktu_sop" class="form-control" placeholder="Waktu SOP Pelayanan"/>
                            <span class="input-group-addon">Hari</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Biaya <small class="text-danger">*</small></label>
                        <input type="number" name="biaya" class="form-control" placeholder="Biaya"/>
                        <small>Apabila dibawah 100 akan dikonversi menjadi presentase. Apabila diatas 100 maka akan dikonversi menjadi Rupiah</small>
                    </div>
                    <div class="form-group">
                        <label>Jenis Pelayanan <small class="text-danger">*</small></label>
                        <select name="jenis" class="form-control">
                            <option value="">-- Pilih Jenis Pelayanan --</option>
                            <option value="external">Eksternal - Stakeholder Berada Diluar Lingkup KPKNL</option>
                            <option value="internal">Internal - Stakeholder Berada Didalam Lingkup KPKNL</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status Pelayanan <small class="text-danger">*</small></label><br/>
                        <input type="radio" name="status" value="0"> Non-Aktif &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="status" value="1"> Aktif
                    </div>
                    <div class="form-group">
                        <label>Keterangan <small class="text-danger">*</small></label>
                        <textarea class="form-control" name="keterangan" id="ckeditor"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-save"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
