@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h3>Manajemen Pelayanan <small>KPKNL Biak</small></h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li>Delete</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Pelayanan - {{ $dataPelayanan->pelayanan }}</h4>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <form action="{{ route('internal-kpknl.manajemen-pelayanan.delete.submit') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan->id) }}"/>
                    <p>Apakah anda yakin akan menghapus data pelayanan <b>{{ $dataPelayanan->pelayanan }}</b>?</p>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-trash"></i> Iya</button>
                        <a href="{{ route("internal-kpknl.manajemen-pelayanan.index") }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-crosshairs"></i> Tidak</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
