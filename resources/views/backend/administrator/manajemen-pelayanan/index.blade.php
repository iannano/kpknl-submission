@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Pelayanan <small>KPKNL Biak</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manajemen Pelayanan</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar Pelayanan</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-pelayanan.create") }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Pelayanan Baru</a>
                </div>
            </div>
            <div class="box-body">
                @if (session('information'))
                    <div class="alert alert-success">
                        {{ session('information') }}
                    </div>
                @endif
                <table class="table table-responsive table-striped table-bordered" id="table-pelayanan">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pelayanan</th>
                            <th>Seksi PIC</th>
                            <th>Waktu SOP</th>
                            <th>Biaya</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listPelayanan as $pelayanan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $pelayanan->pelayanan }}</td>
                            <td>{{ $pelayanan->seksi->seksi }}</td>
                            <td>{{ $pelayanan->waktu_sop }} Hari</td>
                            @if($pelayanan->biaya <= 0)
                                <td><label class="label label-primary">Tidak Ada Biaya</label>
                            @elseif($pelayanan->biaya > 0 || $pelayanan->biaya <= 100)
                                <td><label class="label label-warning">{{ $pelayanan->biaya."%" }}</label>
                            @else
                                <td><label class="label label-danger">Rp{{ number_format($pelayanan->biaya,2) }}</label>
                            @endif
                            <td>
                                <a href="{{ route("internal-kpknl.manajemen-pelayanan.update", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($pelayanan->id)]) }}" class="btn btn-warning btn-xs btn-flat" style="margin-bottom: 0.2em;"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{ route("internal-kpknl.manajemen-pelayanan.delete", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($pelayanan->id)]) }}" class="btn btn-danger btn-xs btn-flat" style="margin-bottom: 0.2em;"><i class="fa fa-trash"></i> Hapus</a>
                                <a href="{{ route("internal-kpknl.manajemen-prosedur.index", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($pelayanan->id)]) }}" class="btn btn-primary btn-xs btn-flat" style="margin-bottom: 0.2em;"><i class="fa fa-star-half-empty"></i> Manajemen Prosedur</a>
                                <a href="{{ route('internal-kpknl.manajemen-dokumen.index', ['id'=>\Illuminate\Support\Facades\Crypt::encrypt($pelayanan->id)]) }}" class="btn btn-primary btn-xs btn-flat" style="margin-bottom: 0.2em;"><i class="fa fa-star-half-empty"></i> Manajemen Dokumen</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
