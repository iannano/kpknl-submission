@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Pegawai <small>Form Create</small></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Dashboard</li>
            <li class="breadcrumb-item">Manajemen Pegawai</li>
            <li>Create Form</li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <form method="POST" action="{{ route("internal-kpknl.manajemen-pegawai.create.submit") }}">
                    @csrf
                    <div class="form-group">
                        <label>Seksi <small class="text-danger">*</small></label>
                        <select class="form-control" name="seksi">
                            <option value="">--- Pilih Seksi ---</option>
                            @foreach($listSeksi as $seksi)
                                <option value="{{ $seksi->id }}">{{ $seksi->seksi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Role <small class="text-danger">*</small></label>
                        <select class="form-control" name="role">
                            <option value="">--- Pilih Role ---</option>
                            <option value="admin">Admin</option>
                            <option value="kepala_kantor">Kepala Kantor</option>
                            <option value="kepala_seksi">Kepala Seksi</option>
                            <option value="pegawai">Pegawai</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap <small class="text-danger">*</small></label>
                        <input type="text" name="nama" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Username <small class="text-danger">*</small></label>
                        <input type="text" name="username" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Password <small class="text-danger">*</small></label>
                        <input type="password" name="password" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Re-Password <small class="text-danger">*</small></label>
                        <input type="password" name="re-password" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        <a href="#" class="btn btn-warning btn-flat"><i class="fa fa-backward"></i> Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
