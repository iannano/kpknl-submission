@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Pegawai <small>Form Update</small></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Dashboard</li>
            <li class="breadcrumb-item">Manajemen Pegawai</li>
            <li>Create Form</li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <form method="POST" action="{{ route("internal-kpknl.manajemen-pegawai.update.submit") }}">
                    @csrf
                    <input type="hidden" name="id-pegawai" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPegawai['id']) }}"/>
                    <div class="form-group">
                        <label>Seksi <small class="text-danger">*</small></label>
                        <select class="form-control" name="seksi">
                            <option value="">--- Pilih Seksi ---</option>
                            @foreach($listSeksi as $seksi)
                                <option value="{{ $seksi->id }}" @if($seksi->id == $dataPegawai['seksi']['id']) selected @endif>{{ $seksi->seksi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Role <small class="text-danger">*</small></label>
                        <select class="form-control" name="role">
                            <option value="">--- Pilih Role ---</option>
                            <option value="admin" @if("admin" == $dataPegawai['role']) selected @endif>Admin</option>
                            <option value="kepala_kantor" @if("kepala_kantor" == $dataPegawai['role']) selected @endif>Kepala Kantor</option>
                            <option value="kepala_seksi" @if("kepala_seksi" == $dataPegawai['role']) selected @endif>Kepala Seksi</option>
                            <option value="pegawai" @if("pegawai" == $dataPegawai['role']) selected @endif>Pegawai</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap <small class="text-danger">*</small></label>
                        <input type="text" name="nama" value="{{ $dataPegawai['nama'] }}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Username <small class="text-danger">*</small></label>
                        <input type="text" name="username" value="{{ $dataPegawai['username'] }}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a href="{{ route('internal-kpknl.manajemen-pegawai.index') }}" class="btn btn-warning btn-flat"><i class="fa fa-backward"></i> Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
