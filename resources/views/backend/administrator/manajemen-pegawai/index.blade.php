@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Pegawai</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Dashboard</li>
            <li>Manajemen Pegawai</li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar Pegawai</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-pegawai.create") }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Pegawai</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered" id="table-pegawai">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>Seksi</th>
                            <th>Username</th>
                            <th width="20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listPegawai as $pegawai)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $pegawai->nama }}</td>
                                <td>{{ $pegawai->seksi->seksi }}</td>
                                <td>{{ $pegawai->username }}</td>
                                <td>
                                    <a href="{{ route('internal-kpknl.manajemen-pegawai.update', ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($pegawai->id)]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{ route('internal-kpknl.manajemen-pegawai.delete', ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($pegawai->id)]) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
