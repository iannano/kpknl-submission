@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Pegawai <small>Form Create</small></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Dashboard</li>
            <li class="breadcrumb-item">Manajemen Pegawai</li>
            <li>Delete Form</li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <p>Apakah anda yakin akan menghapus data pegawai <b>{{ $dataPegawai['nama'] }}</b>?</p>
                <form method="POST" action="{{ route("internal-kpknl.manajemen-pegawai.delete.submit") }}">
                    @csrf
                    <input type="hidden" name="id-pegawai" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPegawai['id']) }}"/>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i> Hapus</button>
                        <a href="#" class="btn btn-warning btn-flat"><i class="fa fa-backward"></i> Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
