@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Satuan Kerja <small>Form Update</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <form method="POST" action="{{ route("internal-kpknl.manajemen-satuan-kerja.update.submit") }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataSatuanKerja['id']) }}"/>
                    <div class="form-group">
                        <label>Satuan Kerja <small class="text-danger">*</small></label>
                        <input type="text" name="satuan-kerja" class="form-control" value="{{ $dataSatuanKerja['satuan_kerja'] }}" required/>
                    </div>
                    <div class="form-group">
                        <label>Email <small class="text-danger">*</small></label>
                        <input type="email" name="email" class="form-control" value="{{ $dataSatuanKerja['email'] }}" required/>
                    </div>
                    <div class="form-group">
                        <label>Telephone <small class="text-danger">*</small></label>
                        <input type="text" name="phone" class="form-control" value="{{ $dataSatuanKerja['phone'] }}" required/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route("internal-kpknl.manajemen-satuan-kerja.index") }}" class="btn btn-warning"><i class="fa fa-backward"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
