@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Satuan Kerja <small>KPKNL Biak</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar Dokumen</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-satuan-kerja.create") }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Satuan Kerja</a>
                </div>
            </div>
            <div class="box-body">
                @if (session('information'))
                    <div class="alert alert-success">
                        {{ session('information') }}
                    </div>
                @endif
                <table class="table table-bordered table-striped" id="satuan-kerja">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Satuan Kerja</th>
                            <th>Nomor Telp</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listSatuanKerja as $satuanKerja)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $satuanKerja->satuan_kerja }}</td>
                                <td>{{ $satuanKerja->phone }}</td>
                                <td>{{ $satuanKerja->email }}</td>
                                <td>
                                    <a href="{{ route('internal-kpknl.manajemen-satuan-kerja.update',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($satuanKerja->id)]) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{ route('internal-kpknl.manajemen-satuan-kerja.delete',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($satuanKerja->id)]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Hapus</a>
                                    <a href="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.index',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($satuanKerja->id)]) }}" class="btn btn-primary btn-xs"><i class="fa fa-star"></i> Manajamen PIC</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
