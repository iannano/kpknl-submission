@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Satuan Kerja <small>Form Update</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <p>Apakah anda yakin untuk menghapus data <b>{{ $dataSatuanKerja['satuan_kerja'] }}</b> ?</p>
                <form method="POST" action="{{ route("internal-kpknl.manajemen-satuan-kerja.delete.submit") }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataSatuanKerja['id']) }}"/>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    <a href="{{ route('internal-kpknl.manajemen-satuan-kerja.index') }}" class="btn btn-warning"><i class="fa fa-backward"></i> Kembali</a>
                </form>
            </div>
        </div>
    </div>
@endsection
