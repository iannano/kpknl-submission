@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen PIC Satuan Kerja <small>KPKNL Biak</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar PIC Satuan Kerja</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-pic-satuan-kerja.create",['id'=>\Illuminate\Support\Facades\Crypt::encrypt($idSatuanKerja)]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Tambah PIC Satuan Kerja</a>
                </div>
            </div>
            <div class="box-body">
                @if (session('information'))
                    <div class="alert alert-success">
                        {{ session('information') }}
                    </div>
                @endif
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Seksi</th>
                        <th>Pegawai</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listPicSatuanKerja as $picSatuanKerja)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $picSatuanKerja->seksi->seksi }}</td>
                            <td>{{ $picSatuanKerja->pegawai->nama }}</td>
                            <td>
                                <a href="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.update',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($picSatuanKerja->id)]) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                <a href="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.delete',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($picSatuanKerja->id)]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
