@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen PIC Satuan Kerja <small>KPKNL Biak</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Penentuan PIC Satuan Kerja</h4>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('information') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.create.submit') }}">
                    @csrf
                    <input type="hidden" name="id-satuan-kerja" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($idSatuanKerja) }}"/>
                    <div class="form-group">
                        <label>Seksi <small class="text-danger">*</small></label>
                        <select name="id-seksi" class="form-control" id="seksi">
                            <option value="">--- Pilih Seksi ---</option>
                            @foreach($listSeksi as $seksi)
                                <option value="{{ $seksi->id }}">{{ $seksi->seksi }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pegawai <small class="text-danger">*</small></label>
                        <select name="id-pegawai" class="form-control" id="pegawai" disabled="disabled">
                            <option value="">--- Pilih Pegawai ---</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('internal-kpknl.manajemen-satuan-kerja.index') }}" class="btn btn-warning"><i class="fa fa-undo"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
