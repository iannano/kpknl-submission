@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen PIC Satuan Kerja <small>Form Hapus</small></h1>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('information') }}
                    </div>
                @endif
                <p>Apakah anda yakin akan menghapus <b>{{ $dataPicSatuanKerja['pegawai']['nama'] }}</b> sebagai PIC <b>Seksi {{ $dataPicSatuanKerja['seksi']['seksi'] }}</b> dari Satuan Kerja <b>{{ $dataPicSatuanKerja['satuankerja']['satuan_kerja'] }}</b>?</p>
                <form method="POST" action="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.delete.submit') }}">
                    @csrf
                    <input type="hidden" name="id-satuan-kerja" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPicSatuanKerja['satuankerja']['id']) }}"/>
                    <input type="hidden" name="id-pic-satuan-kerja" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPicSatuanKerja['id']) }}"/>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                        <a href="{{ route('internal-kpknl.manajemen-pic-satuan-kerja.index',["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dataPicSatuanKerja['satuankerja']['id'])]) }}" class="btn btn-warning"><i class="fa fa-undo"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
