@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Update Dokumen</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manajemen Pelayanan</a></li>
            <li><a href="#">Manajemen Dokumen</a></li>
            <li><a href="#">Update</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Dokumen Baru</h4>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-success">
                        {{ session('error') }}
                    </div>
                @endif
                <form method="POST" action="{{ route("internal-kpknl.manajemen-dokumen.update.submit") }}">
                    @csrf
                    <input type="hidden" name="idPelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan[0]["id"]) }}"/>
                    <input type="hidden" name="idDokumen" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataDokumen->id) }}"/>
                    <div class="form-group">
                        <label>Nama Dokumen <small class="text-danger">*</small></label>
                        <input type="text" name="dokumen" class="form-control" value="{{ $dataDokumen->dokumen }}" placeholder="Nama Dokumen"/>
                    </div>
                    <div class="form-group">
                        <label>Keterangan <small class="text-danger">*</small></label>
                        <textarea id="ckeditor" name="keterangan" class="form-control">{!! $dataDokumen->keterangan !!}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Submit</button>
                        <a href="{{ route("internal-kpknl.manajemen-dokumen.index",["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan[0]["id"])]) }}" class="btn btn-warning"><i class="fa fa-backward"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
