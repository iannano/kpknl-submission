@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Dokumen <small>{{ $dataPelayanan["pelayanan"] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manajemen Pelayanan</a></li>
            <li><a href="#">Manajemen Dokumen</a></li>
            <li><a href="#">Index</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar Dokumen</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-dokumen.create",["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan["id"])]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Dokumen</a>
                </div>
            </div>
            <div class="box-body">
                @if (session('information'))
                    <div class="alert alert-success">
                        {{ session('information') }}
                    </div>
                @endif
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Dokumen</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listDokumen as $dokumen)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $dokumen->dokumen }}</td>
                                <td>{!! $dokumen->keterangan !!}</td>
                                <td>
                                    <a href="{{ route("internal-kpknl.manajemen-dokumen.update",["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dokumen->id)]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                    <a href="{{ route("internal-kpknl.manajemen-dokumen.delete", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dokumen->id)]) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
