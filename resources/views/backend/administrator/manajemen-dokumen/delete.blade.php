@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Hapus Dokumen <small>{{ $dataPelayanan[0]["pelayanan"] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manajemen Pelayanan</a></li>
            <li><a href="#">Menejemen Dokumen</a></li>
            <li>Hapus Dokumen</li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Dokumen Baru</h4>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-success">
                        {{ session('error') }}
                    </div>
                @endif
                <form method="POST" action="{{ route("internal-kpknl.manajemen-dokumen.delete.submit") }}">
                    @csrf
                    <input type="hidden" name="idDokumen" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataDokumen["id"]) }}"/>
                    <input type="hidden" name="idPelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan[0]["id"]) }}"/>
                    <p>Apakah anda yakin untuk melakukan penghapusan dokumen <b>{{ $dataDokumen["dokumen"] }}</b> dalam <b>{{ $dataPelayanan[0]["pelayanan"] }}</b>?</p>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Iya</button>
                        <a href="{{ route("internal-kpknl.manajemen-dokumen.index",["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan[0]["id"])]) }}" class="btn btn-warning"><i class="fa fa-backward"></i> Tidak</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
