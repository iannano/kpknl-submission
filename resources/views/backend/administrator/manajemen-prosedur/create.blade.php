@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Prosedur <small>{{ $dataPelayanan['pelayanan'] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-prosedur.index', ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan['id'])]) }}">Manajemen Prosedur</a></li>
            <li>Create</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Daftar Prosedur</h4>
            </div>
            <div class="box-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <form method="post" action="{{ route("internal-kpknl.manajemen-prosedur.create.submit") }}">
                    @csrf
                    <input type="hidden" name="id_pelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan['id']) }}"/>
                    <div class="form-group">
                        <label>Nama Prosedur</label>
                        <input type="text" name="prosedur" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Tahapan</label>
                        <input type="number" name="tahapan" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
