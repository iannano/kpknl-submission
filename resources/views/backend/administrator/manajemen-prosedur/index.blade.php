@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Prosedur <small>{{ $dataPelayanan['pelayanan'] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li>Manajemen Prosedur</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="box-header">
                    <h4 class="box-title">Daftar Prosedur</h4>
                    <div class="box-tools pull-right">
                        <a href="{{ route("internal-kpknl.manajemen-prosedur.create", ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan["id"])]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Prosedur Baru</a>
                    </div>
                </div>
                <div class="box-body">
                    @if (session('information'))
                        <div class="alert alert-success">
                            {{ session('information') }}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Prosedur</th>
                                <th>Tahapan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listProsedur as $prosedur)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $prosedur->prosedur }}</td>
                                    <td>{{ $prosedur->tahapan }}</td>
                                    <td>
                                        <a href="{{ route("internal-kpknl.manajemen-prosedur.update", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($prosedur->id)]) }}" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-edit"></i> </a>
                                        <a href="{{ route("internal-kpknl.manajemen-prosedur.delete", ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($prosedur->id)]) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
