@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Prosedur <small>{{ $dataProsedur['prosedur'] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-prosedur.index', ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan['id'])]) }}">Manajemen Prosedur</a></li>
            <li>Update</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h4 class="box-title">Update Prosedur</h4>
                <div class="box-tools pull-right">
                    <a href="{{ route("internal-kpknl.manajemen-prosedur.index", ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan["id"])]) }}" class="btn btn-warning btn-sm btn-flat"><i class="fa fa-backward"></i> Kembali</a>
                </div>
            </div>
            <div class="box-body">
                <form method="post" action="{{ route("internal-kpknl.menejemen-prosedur.update.submit") }}">
                    @csrf
                    <input type="hidden" name="id_prosedur" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataProsedur['id']) }}"/>
                    <input type="hidden" name="id_pelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan['id']) }}">
                    <div class="form-group">
                        <label>Prosedur</label>
                        <input type="text" name="prosedur" value="{{ $dataProsedur->prosedur }}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Tahapan</label>
                        <input type="number" name="tahapan" value="{{ $dataProsedur->tahapan }}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
