@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Manajemen Prosedur <small>{{ $dataPelayanan['pelayanan'] }}</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}">Manajemen Pelayanan</a></li>
            <li><a href="{{ route('internal-kpknl.manajemen-prosedur.index', ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan['id'])]) }}">Manajemen Prosedur</a></li>
            <li>Delete</li>
        </ol>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="box-header">
                    <h4 class="box-title">Hapus</h4>
                    <div class="box-tools pull-right">
                        <a href="{{ route("internal-kpknl.manajemen-prosedur.create", ["id" => \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan["id"])]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Buat Prosedur Baru</a>
                    </div>
                </div>
                <div class="box-body">
                    @if (session('information'))
                        <div class="alert alert-success">
                            {{ session('information') }}
                        </div>
                    @endif
                    <p>Apakah anda yakin untuk menghapus prosedur <b>{{ $dataProsedur->prosedur }}</b>?</p>
                    <form method="post" action="{{ route("internal-kpknl.manajemen-prosedur.delete.submit") }}">
                        @csrf
                        <input type="hidden" name="id_pelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan->id) }}"/>
                        <input type="hidden" name="id_prosedur" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataProsedur->id) }}"/>
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                            <a href="{{ route('internal-kpknl.manajemen-prosedur.index', ["id"=>\Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan->id)]) }}" class="btn btn-warning"><i class="fa fa-backward"></i> Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
