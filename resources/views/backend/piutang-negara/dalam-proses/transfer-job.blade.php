@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Dalam Proses <small>Transfer Pelayanan</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">PKN</a></li>
            <li><a href="#">Permohonan Masuk</a></li>
            <li><a href="#">List</a></li>
        </ol>
    </div>
    <div class="content">
        @if (session('information'))
            <div class="alert alert-success">
                {{ session('information') }}
            </div>
        @endif
        <div class="box">
            <div class="box-body">
                <h4>Data Pelayanan</h4>
                <table class="table-responsive">
                    <tr>
                        <td>Satuan Kerja Pemohon</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['satuankerja']['satuan_kerja'] }}</b></td>
                    </tr>
                    <tr>
                        <td>Jenis Permohonan</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['pelayanan'] }}</b></td>
                    </tr>
                    <tr>
                        <td>Nomor Satuan Kerja</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['satuankerja']['phone'] }}</b></td>
                    </tr>
                    <tr>
                        <td>SOP Pelaksanaan</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['waktu_sop'] }} Hari</b></td>
                    </tr>
                    <tr>
                        <td>Waktu Mulai</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['waktu_sop'] }} Hari</b></td>
                    </tr>
                    <tr>
                        <td>Waktu Tersisa</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['waktu_sop'] }} Hari</b></td>
                    </tr>
                </table>
                <hr/>
                <form method="POST" action="{{ route("internal-kpknl.piutang-negara.dalam-proses.transfer-job.submit") }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataDetailPelayanan['id']) }}"/>
                    <div class="form-group">
                        <label>Pelayanan Penilaian <small class="text-danger">*</small></label>
                        <select class="form-control" name="id-pelayanan" required>
                            <option value="#">--- Pilih Pelayanan Penilaian ---</option>
                            @foreach($listPelayananPenilai as $pelayananPenilai)
                                <option value="{{ $pelayananPenilai->id }}">{{ $pelayananPenilai->pelayanan }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-flat btn-primary">Transfer Ke Penilai</button>
                        <a href="{{ route('internal-kpknl.piutang-negara.dalam-proses.current-step',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($dataDetailPelayanan['id'])]) }}" type="submit" class="btn btn-flat btn-warning">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
