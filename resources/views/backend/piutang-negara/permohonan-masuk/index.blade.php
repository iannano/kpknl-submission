@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Permohonan Masuk <small>Piutang Negara</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">PKN</a></li>
            <li><a href="#">Permohonan Masuk</a></li>
            <li><a href="#">Index</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h4 class="box-title">Daftar Dokumen</h4>
                <div class="box-tools pull-right">
                    <a href="#" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> Input Permohonan</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped table-responsive" id="table-permohonan-masuk">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Satuan Kerja</th>
                            <th>Pelayanan</th>
                            <th>Waktu Dokumen Masuk</th>
                            <th>PIC KPKNL</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($listDetailPelayanan as $detailPelayanan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $detailPelayanan->satuan_kerja }}</td>
                            <td>{{ $detailPelayanan->pelayanan }}</td>
                            <td>{{ $detailPelayanan->created_at }}</td>
                            <td>
                                @if($detailPelayanan->nama !== NULL)
                                    <label class="label label-primary">{{ $detailPelayanan->nama }}</label>
                                @else
                                    <label class="label label-danger">Belum Ada PIC</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route("internal-kpknl.piutang-negara.permohonan-masuk.assign-job",["id"=>\Illuminate\Support\Facades\Crypt::encrypt($detailPelayanan->id)]) }}" class="btn btn-primary btn-xs">Tugaskan Ke Saya</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
