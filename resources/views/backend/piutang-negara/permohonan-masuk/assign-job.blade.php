@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Permohonan Masuk <small>Piutang Negara</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">PKN</a></li>
            <li><a href="#">Permohonan Masuk</a></li>
            <li><a href="#">Index</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-header">
                Detail Permohonan Masuk
            </div>
            <div class="box-body">
                <table class="table-responsive">
                    <tr>
                        <td>Satuan Kerja Pemohon</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['satuankerja']['satuan_kerja'] }}</b></td>
                    </tr>
                    <tr>
                        <td>Jenis Permohonan</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['pelayanan'] }}</b></td>
                    </tr>
                    <tr>
                        <td>Nomor Satuan Kerja</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['satuankerja']['phone'] }}</b></td>
                    </tr>
                    <tr>
                        <td>SOP Pelaksanaan</td>
                        <td>:</td>
                        <td><b>{{ $dataDetailPelayanan['pelayanan']['waktu_sop'] }} Hari</b></td>
                    </tr>
                </table>
                <hr/>
                <p>Apakah anda yakin untuk menerima tugas?</p>
                <form method="POST" action="{{ route('internal-kpknl.piutang-negara.permohonan-masuk.assign-job.submit') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataDetailPelayanan['id']) }}"/>
                    <button type="submit" class="btn btn-primary">Iya</button>
                    <a href="{{ route("internal-kpknl.piutang-negara.permohonan-masuk.index") }}" class="btn btn-danger">Tidak</a>
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection
