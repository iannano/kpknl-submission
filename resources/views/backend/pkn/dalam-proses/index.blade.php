@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Dalam Proses <small>PKN</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">PKN</a></li>
            <li><a href="#">Permohonan Masuk</a></li>
            <li><a href="#">List</a></li>
        </ol>
    </div>
    <div class="content">
        <div class="box">
            <div class="box-body">
                <table class="table table-responsive table-bordered" id="table-dalam-proses">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Satuan Kerja</th>
                            <th>Tiket</th>
                            <th>Pelayanan</th>
                            <th>Waktu Tersisa</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listDetailPelayanan as $detailPelayanan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $detailPelayanan['satuankerja']['satuan_kerja'] }}</td>
                            <td>{{ $detailPelayanan['tiket'] }}</td>
                            <td>{{ $detailPelayanan['pelayanan']['pelayanan'] }}</td>
                            <td>
                                @if($detailPelayanan['status'] !== 3)
                                    @php
                                        if($detailPelayanan['waktu_berakhir'] !== NULL)
                                        {
                                            $now = \Carbon\Carbon::now();
                                            $end = $detailPelayanan['waktu_berakhir'];
                                            echo $now->diffAsCarbonInterval($end);
                                        }
                                        else
                                        {
                                            echo "<label class='label label-warning'>Berhenti Sementara</label>";
                                        }
                                    @endphp
                                @else
                                    <label class="label label-primary">Selesai</label>
                                @endif
                            </td>
                            <td>
                                @if($detailPelayanan['status'] === 0)
                                    <label class="label label-danger">Belum Diproses</label>
                                @elseif($detailPelayanan['status'] === 1)
                                    <label class="label label-primary">Sedang Berjalan</label>
                                @elseif($detailPelayanan['status'] === 2)
                                    <label class="label label-warning">Dalam Proses Penilaian</label>
                                @elseif($detailPelayanan['status'] === 3)
                                    <label class="label label-success">Selesai</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('internal-kpknl.pkn.dalam-proses.current-step',['id'=>\Illuminate\Support\Facades\Crypt::encrypt($detailPelayanan['id'])]) }}" class="btn btn-primary btn-xs">Prosedur</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
