<section class="sidebar" style="height: auto;">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ url('assets/images/logo-kementerian-keuangan-01.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ session('access-internal')['nama'] }}</p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @if((int)session('access-internal')['seksi'] === 2 || (int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>PKN</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('internal-kpknl.pkn.permohonan-masuk.index') }}"><i class="fa fa-circle-o"></i> Permohonan Masuk</a></li>
                <li><a href="{{ route('internal-kpknl.pkn.dalam-proses.index') }}"><i class="fa fa-circle-o"></i> Dalam Proses PKN</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Selesai</a></li>
            </ul>
        </li>
        @endif
        @if((int)session('access-internal')['seksi'] === 3 || (int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Penilaian</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route("internal-kpknl.penilaian.permohonan-masuk.index") }}"><i class="fa fa-circle-o"></i> Permohonan Masuk</a></li>
                <li><a href="{{ route("internal-kpknl.penilaian.dalam-proses.index") }}"><i class="fa fa-circle-o"></i> Dalam Proses Penilaian</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Selesai</a></li>
            </ul>
        </li>
        @endif
        @if((int)session('access-internal')['seksi'] === 5 || (int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Lelang</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('internal-kpknl.lelang.permohonan-masuk.index') }}"><i class="fa fa-circle-o"></i> Permohonan Masuk</a></li>
                <li><a href="{{ route('internal-kpknl.lelang.dalam-proses.index') }}"><i class="fa fa-circle-o"></i> Dalam Proses PKN</a></li>
            </ul>
        </li>
        @endif
        @if((int)session('access-internal')['seksi'] === 4 || (int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Piutang Negara</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('internal-kpknl.piutang-negara.permohonan-masuk.index') }}"><i class="fa fa-circle-o"></i> Permohonan Masuk</a></li>
                <li><a href="{{ route('internal-kpknl.piutang-negara.dalam-proses.index') }}"><i class="fa fa-circle-o"></i> Dalam Proses PKN</a></li>
            </ul>
        </li>
        @endif
        @if((int)session('access-internal')['seksi'] === 1 || (int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Umum</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Penerbitan Surat Tugas</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Tidak Mengisi Daftar Hadir</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Kenaikan Gaji Berkala</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Kenaikan Grade</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Kenaikan Golongan</a></li>
            </ul>
        </li>
        @endif
        @if((int)session('access-internal')['seksi'] === 1)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Administrator</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('internal-kpknl.manajemen-pegawai.index') }}"><i class="fa fa-circle-o"></i> Manajemen User</a></li>
                <li><a href="{{ route('internal-kpknl.manajemen-satuan-kerja.index') }}"><i class="fa fa-circle-o"></i> Manajemen Satuan Kerja</a></li>
                <li><a href="{{ route('internal-kpknl.manajemen-pelayanan.index') }}"><i class="fa fa-circle-o"></i> Manajemen Pelayanan</a></li>
            </ul>
        </li>
        @endif
        <li class="header">LINK EXTERNAL APPLICATION</li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-circle-o"></i>
                <span>Aplikasi DJKN</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="http://smart.djkn.kemenkeu.go.id" target="_blank"><i class="fa fa-circle-o"></i> SMARt</a></li>
                <li><a href="http://10.242.151.21/absensi/login" target="_blank"><i class="fa fa-circle-o"></i> Dianas</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-circle-o"></i>
                <span>E-Prime</span>
                <span class="pull-right-container">
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="http://hris.e-prime.kemenkeu.go.id" target="_blank"><i class="fa fa-circle-o"></i> HRIS</a></li>
                <li><a href="http://e-performance.kemenkeu.go.id" target="_blank"><i class="fa fa-circle-o"></i> E-Peformance</a></li>
            </ul>
        </li>
    </ul>
</section>
