@extends("layout.backend.main-layout")
@section("content")
    <div class="content-header">
        <h1>Dalam Proses <small>Penilaian</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('internal-kpknl.dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Penilaian</a></li>
            <li><a href="#">Dalam Proses</a></li>
            <li><a href="#">List</a></li>
        </ol>
    </div>
    <div class="content">
        @if (session('information'))
            <div class="alert alert-success">
                {{ session('information') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        Data Pemohon
                    </div>
                    <div class="box-body">
                        <table class="table-responsive">
                            <tr>
                                <td>Satuan Kerja Pemohon</td>
                                <td>:</td>
                                <td><b>{{ $dataDetailPelayanan['satuankerja']['satuan_kerja'] }}</b></td>
                            </tr>
                            <tr>
                                <td>Jenis Permohonan</td>
                                <td>:</td>
                                <td><b>{{ $dataDetailPelayanan['pelayanan']['pelayanan'] }}</b></td>
                            </tr>
                            <tr>
                                <td>Nomor Satuan Kerja</td>
                                <td>:</td>
                                <td><b>{{ $dataDetailPelayanan['satuankerja']['phone'] }}</b></td>
                            </tr>
                            <tr>
                                <td>SOP Pelaksanaan</td>
                                <td>:</td>
                                <td><b>{{ $dataDetailPelayanan['pelayanan']['waktu_sop'] }} Hari</b></td>
                            </tr>
                            <tr>
                                <td>Waktu Mulai</td>
                                <td>:</td>
                                <td><b>{{ date_format(date_create($dataDetailPelayanan['waktu_mulai']),'d-m-Y d:i:s') }}</b></td>
                            </tr>
                            <tr>
                                <td>Waktu Berakhir</td>
                                <td>:</td>
                                <td><b>{{ date_format(date_create($dataDetailPelayanan['waktu_berakhir']),'d-m-Y d:i:s') }}</b></td>
                            </tr>
                            <tr>
                                <td>Waktu Tersisa</td>
                                <td>:</td>
                                <td>
                                    <b>
                                        @php
                                            $now = \Carbon\Carbon::now();
                                            $end = $dataDetailPelayanan['waktu_berakhir'];
                                            echo $now->diffAsCarbonInterval($end);
                                        @endphp
                                    </b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        Tahapan Pelayanan
                    </div>
                    <div class="box-body">
                        <form method="POST" action="{{ route("internal-kpknl.penilaian.dalam-proses.current-step.submit") }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataDetailPelayanan['id']) }}">
                            @foreach($listProsedurPelayanan as $prosedurPelayanan)
                                <div class="form-group">
                                    <input type="checkbox"
                                           name="prosedur[{{$prosedurPelayanan->tahapan}}]"
                                           value="{{ $prosedurPelayanan->id }}"
                                           class="current-step"
                                           @if(in_array($prosedurPelayanan->id, $arrayIdDetailProsedurPelayanan))checked="checked" @endif/>
                                    {{ $prosedurPelayanan->prosedur }}
                                </div>
                            @endforeach
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat btn-sm">Submit</button>
                                <a href="{{ route("internal-kpknl.penilaian.dalam-proses.index") }}" class="btn btn-warning btn-flat btn-sm">Kembali</a>
                                <a href="{{ route("internal-kpknl.penilaian.dalam-proses.job-done.submit",['id'=>\Illuminate\Support\Facades\Crypt::encrypt($dataDetailPelayanan['id'])]) }}" class="btn btn-danger btn-flat btn-sm">Selesai</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
