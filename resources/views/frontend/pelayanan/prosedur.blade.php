@extends("layout.frontend.main-layout")
@section("content")
    <div class="container">
        <h3>{{ $dataPelayanan[0]['pelayanan'] }}</h3>
        <h4>Penjelasan:</h4>
        <p>{!! $dataPelayanan[0]['keterangan'] !!}</p>
        @if((int)$dataPelayanan[0]['biaya'] === 0)
            <p>Tidak ada biaya administrasi</p>
        @elseif((int)$dataPelayanan[0]['biaya'] > 0 && (int)$dataPelayanan[0]['biaya'] <= 100)
            <p>Biaya Administrasi: {{ $dataPelayanan[0]['biaya'] }}%</p>
        @else
            <p>Biaya Administrasi: Rp{{ number_format($dataPelayanan[0]['biaya'],2) }}</p>
        @endif
        <p>Waktu SOP: {{ $dataPelayanan[0]['waktu_sop'] }} Hari Kerja</p>
        <h4>Prosedur:</h4>
        @foreach($listProsedurPelayanan as $prosedurPelayanan)
            <p>{{$loop->iteration}}. {{ ucwords(strtolower($prosedurPelayanan->prosedur)) }}</p>
        @endforeach
        <h4>Dokumen Pelayanan:</h4>
        @foreach($listDokumen as $dokumen)
            <p>{{ $loop->iteration }}. {{ ucwords(strtolower($dokumen->dokumen)) }}</p>
        @endforeach
        @if(session('access-satuan-kerja')['id'])
        <br/>
        <form method="POST" action="{{ route('pelayanan.prosedur.submit',[$dataPelayanan[0]["seksi"]["slug"], $dataPelayanan[0]['slug']]) }}">
            @csrf
            <input type="hidden" name="idPelayanan" value="{{ \Illuminate\Support\Facades\Crypt::encrypt($dataPelayanan[0]["id"]) }}"/>
            <button type="submit" class="btn btn-primary btn-sm">Ajukan Permohonan</button>
        </form>
        @endif
    </div>
@endsection
