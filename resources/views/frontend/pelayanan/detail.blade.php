@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container-fluid main-container" style="padding-left: 0 !important;; padding-right: 0 !important;">
            <img src="{{ url('assets/images/placeholder.jpg') }}" class="img-responsive" style="width: 100%; height: 20%;"/>
        </div>
        <div class="container">
            <h4>Jenis Pelayanan</h4>
            <ul>
                @foreach($listPelayanan as $pelayanan)
                <li><a href="{{ url("pelayanan/".$slugseksi."/".$pelayanan->slug) }}">{{ $pelayanan->pelayanan }}</a></li>
                @endforeach
            </ul>
        </div>
    </section>
@endsection
