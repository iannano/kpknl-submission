@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <h4 class="text-center">Pelayanan KPKNL Biak</h4>
            <hr/>
            @foreach($listSeksi as $seksi)
            <div class="col-md-3">
                <a href="{{ url("pelayanan/".$seksi->slug) }}">
                    <img class="img-responsive" src="{{ url('assets/images/placeholder.jpg') }}"/>
                    <h4 class="text-center">{{ $seksi->seksi }}</h4>
                </a>
            </div>
            @endforeach
        </div>
    </section>
@endsection
