@extends("layout.frontend.main-layout")
@section("content")
    <div class="container">
        <div class="page-header">
            <h1 id="timeline">Pelacakan Layanan:</h1>
        </div>
    </div>
    <div class="container">
        <center>
            <h3>{{ $listDetailPelayanan[0]['pelayanan']['pelayanan'] }}</h3>
        </center>
        <div class="col-md-6">
            <h4>Data Pemohon</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p><i class="fa fa-user"></i> {{ $listDetailPelayanan[0]['satuankerja']['satuan_kerja'] }}</p>
                    <p><i class="fa fa-address-book"></i> {{ $listDetailPelayanan[0]['satuankerja']['email'] }}</p>
                    <p><i class="fa fa-phone"></i> {{ $listDetailPelayanan[0]['satuankerja']['phone'] }}</p>
                </div>
            </div>
            <hr/>
            @if($listDetailPelayanan[0]['id_pegawai'] !== NULL)
                <h4>Data PIC KPKNL</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><i class="fa fa-user"></i> {{ $listDetailPelayanan[0]['pegawai']['nama'] }}</p>
                    </div>
                </div>
                <br/>
            @endif
        </div>
        <div class="col-md-6">
            @if(count($listProsedur) === 0)
                <div class="alert alert-danger text-center">
                    <p>Belum Di Proses</p>
                </div>
            @else
                <ul class="timeline">
                    @foreach($listProsedur as $prosedur)
                        <li>
                            <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">{{ $prosedur['prosedurpelayanan']['prosedur'] }}</h4>
                                    <p><small class="text-muted">{{ $prosedur['waktu'] }}</small></p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @if((int)$listDetailPelayanan[0]['status'] === 3)
                        <li>
                            <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">Selesai</h4>
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
            @endif
        </div>
    </div>
@endsection
