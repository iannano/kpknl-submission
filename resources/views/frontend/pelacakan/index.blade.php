@extends("layout.frontend.main-layout")
@section("content")
    <div class="container">
        <h4>Pelacakan</h4>
        <hr/>
        <form method="POST" action="{{ route('pelacakan.index.submit') }}">
            @csrf
            <div class="form-group">
                <label>Nomor Tracking</label>
                <input type="text" name="ticket-code" class="form-control" placeholder="Nomor Tracking"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
