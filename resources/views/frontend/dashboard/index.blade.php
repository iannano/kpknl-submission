@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class="text-center">PROFILE SATUAN KERJA</h5>
                        <i class="fa fa-user"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                        <i class="fa fa-phone"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja']) }} <br/>
                        <i class="fa fa-location-arrow"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja']) }} <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>HISTORY PENGAJUAN SAYA</h5>
                                <table class="table table-striped table-bordered table-responsive" id="table-pengajuan-saya">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pelayanan</th>
                                            <th>Tiket</th>
                                            <th>Pengajuan</th>
                                            <th>Status Permohonan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listDetailPelayanan as $detailPelayanan)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $detailPelayanan['pelayanan']['pelayanan'] }}</td>
                                            <td>{{ $detailPelayanan['tiket'] }}</td>
                                            <td>{{ $detailPelayanan['created_at'] }}</td>
                                            <td>
                                                @if($detailPelayanan['status'] === 0)
                                                    <label class="label label-danger">Belum Diproses</label>
                                                @elseif($detailPelayanan['status'] === 1)
                                                    <label class="label label-primary">Sedang Berjalan</label>
                                                @elseif($detailPelayanan['status'] === 2)
                                                    <label class="label label-warning">Dalam Proses Penilaian</label>
                                                @elseif($detailPelayanan['status'] === 3)
                                                    <label class="label label-success">Selesai</label>
                                                @endif
                                            </td>
                                            <td><a href="#"><button class="btn btn-primary btn-xs btn-flat">Lihat Detail</button></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
