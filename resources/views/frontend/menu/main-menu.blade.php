<div class="top-header">
    <div class="container">
        <div class="col-md-6">
            <img class="home-icon" src="https://www.kemenkeu.go.id/images/icon/ico-home.svg">&nbsp; Jl. Lapangan Bateng Timur No. 2 - 4
        </div>
        <div class="col-md-6">
            <span class="pull-right">1-500-991 (Jam Operasional: Senin - Jum'at : 08.00 - 17.00 WIB)</span> &nbsp;
            <img class="phone-icon pull-right" src="https://www.kemenkeu.go.id/images/icon/ico-phone.svg">
        </div>
    </div>
</div>
<div class="navbar-menu">
    <div class="container">
        <div class="col-md-4">
            <a href="{{url("/")}}">
                <img class="img-responsive" src="{{url("assets/images/logo-kemenkeu-djkn.png")}}">
            </a>
        </div>
        <div class="col-md-8">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('pelayanan') }}">Pelayanan</a></li>
                            <li><a href="{{ route('pelacakan.index') }}">Pelacakan Pengajuan</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
</div>
