@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-sm-3">
                <div class="panel panel-body">
                    <form>
                        <div class="form-group">
                            <label>Kata Kunci</label>
                            <input type="text" name="keyword" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-body">
                    <h3 class="text-center">Daftar Peraturan</h3>
                    @for($i = 0; $i < 5; $i++)
                        <p><b>Nama Peraturan</b></p>
                        <p>Keterangan singkat peraturan</p>
                        <p>Status berlaku peraturan</p>
                        <p>Tanggal Peraturan</p>
                        <p><button class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Unduh</button> | <button class="btn btn-primary btn-xs"><i class="fa fa-bookmark"></i> Pratinjau</button> </p>
                    @endfor
                </div>
            </div>
        </div>
    </section>
@endsection
