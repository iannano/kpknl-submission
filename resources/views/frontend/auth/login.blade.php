@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <h4 class="text-center">Login</h4>
                <hr/>
                @if (session('error'))
                    <div class="alert alert-danger text-center">
                        {{ session('error') }}
                    </div>
                @endif
                <form method="post" action="{{ url('auth/login/submit') }}">
                    @csrf
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <a href="#">Lupa Password?</a>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm col-md-12">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
