@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <h4 class="text-center">Forget Password</h4>
                <hr/>
                <form>
                    <div class="form-group">
                        <label>Username / Email</label>
                        <input type="text" name="username" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <a href="{{ route('forget-password.submit') }}" class="btn btn-primary btn-sm col-md-12">Submit</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
