@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class="text-center">PROFILE SATUAN KERJA</h5>
                        <i class="fa fa-user"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                        <i class="fa fa-phone"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5 class="text-center">UPLOAD DOKUMEN PENGAJUAN PELAYANAN</h5>
                                <p class="text-center"><small>Tahap 2 dari 3</small></p>
                                <p>Pelayanan:</p>
                                <p><b>{{ $dataPelayanan['pelayanan'] }}</b></p>
                                <p>Deksripsi Pelayanan:</p>
                                <p>Penghapusan adalah tindakan menghapus Barang Milik Negara dari daftar barang dengan
                                    menerbitkan keputusan dari pejabat yang berwenang untuk membebaskan Pengguna
                                    Barang dan/atau Kuasa Pengguna Barang dan/atau Pengelola Barang dari tanggung jawab
                                    administrasi dan fisik barang yang berada dalam penguasaannya.</p>
                                <hr/>
                                <form action="{{ route('pengajuan-pelayanan.pelayanan.submit') }}" method="post">
                                    @csrf
                                    @foreach($listDokumen as $dokumen)
                                        <div class="form-group">
                                            <label>{{ $dokumen->dokumen }} <small class="text-danger">*</small></label>
                                            <input type="file" name="file[]" class="form-control" required/>
                                        </div>
                                    @endforeach
                                    <div class="form-group">
                                        <div class="pull-left">
                                            <a href="{{ route("pengajuan-pelayanan.pelayanan") }}" class="btn btn-warning btn-sm"><i class="fa fa-backward"></i> Pilih Pelayanan Lainnya</a>
                                        </div>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary btn-sm">Buat Permohonan <i class="fa fa-forward"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
