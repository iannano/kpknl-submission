@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class="text-center">PROFILE SATUAN KERJA</h5>
                        <i class="fa fa-user"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                        <i class="fa fa-phone"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5 class="text-center">TIKET PENGAJUAN PELAYANAN</h5>
                                <p class="text-center"><small>Tahap 2 dari 2</small></p>
                                <p>Yth. {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }}</p>
                                <p>Terima kasih telah melakukan pengajuan layanan pada KPKNL Biak. </p>
                                <p>Dibawah ini merupakan kode tiket Bapak/Ibu, gunakan kode dibawah untuk melakukan pencarian terhadap tindak lanjut pengajuan layanan yang Bapak/Ibu lakukan</p>
                                <p class="text-center">Kode Pencarian:</p>
                                <h4 class="text-center"><b>{{ $dataTicketCode }}</b></h4>
                                <p>Kami telah mengirimkan kode tersebut kedalam email satuan kerja Bapak/Ibu</p>
                                <a href="{{ route('dashboard.index') }}" class="btn btn-primary btn-sm">Kembali Ke Dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
