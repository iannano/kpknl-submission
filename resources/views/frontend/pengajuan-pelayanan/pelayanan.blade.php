@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class="text-center">PROFILE SATUAN KERJA</h5>
                        <i class="fa fa-user"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                        <i class="fa fa-phone"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5 class="text-center">PILIH PENGAJUAN PELAYANAN</h5>
                                <p class="text-center"><small>Tahap 1 dari 2</small></p>
                                <form action="{{ route('pengajuan-pelayanan.pelayanan.submit') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label>Layanan <small class="text-danger">*</small></label>
                                        <select name="id_pelayanan" class="form-control" required>
                                            <option value="">-- Pilih Pelayanan --</option>
                                            @foreach($listPelayanan as $pelayanan)
                                                @if(Redis::get('id_pelayanan'))
                                                    <option value="{{ $pelayanan->id }}" @if($pelayanan->id == Redis::get('id_pelayanan')) selected @endif>{{ $pelayanan->pelayanan }}</option>
                                                @else
                                                    <option value="{{ $pelayanan->id }}">{{ $pelayanan->pelayanan }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="pull-left">
                                            <a href="{{ route("pengajuan-pelayanan.index") }}" class="btn btn-warning btn-sm"><i class="fa fa-backward"></i> Kembali</a>
                                        </div>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary btn-sm">Lanjut <i class="fa fa-forward"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
