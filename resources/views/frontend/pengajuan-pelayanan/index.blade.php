@extends("layout.frontend.main-layout")
@section("content")
    <section>
        <div class="container">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h5 class="text-center">PROFILE SATUAN KERJA</h5>
                        <i class="fa fa-user"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                        <i class="fa fa-phone"></i> {{ ucwords(session('access-satuan-kerja')['satuan_kerja'])  }} <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @if (session('information'))
                                    <div class="alert alert-success">
                                        {{ session('information') }}
                                    </div>
                                @endif
                                <h5 class="text-center">PENGAJUAN PELAYANAN DISCLAIMER</h5>
                                    <p>Dalam menyambut perubahan industri 4.0, maka KPKNL Biak melakukan sebuah inovasi untuk mempermudah Bapak/Ibu melakukan pelacakan terhadap pelayanan yang Bapak/Ibu ajukan, setiap perkembangan dalam proses pelayanan akan diinformasikan melalui email yang terdaftar dalam satuan kerja dan telegram</p>
                                    <p>Pastikan Bapak/Ibu melakukan add <b>@AsgardBiakBot</b> pada telegram untuk mendapatkan notifikasi melalui telegram Bapak/Ibu</p>
                                    <p><b>Program dalam tahapan uji coba, apabila terdapat kesulitan melakukan akses atau mengajukan pelayanan dapat mengisikan form <a href="https://forms.gle/7RiARow4koK5iDRV6">ini</a></b></p>
                                <p>Terima kasih</p>
                                <a href="{{ route('pengajuan-pelayanan.pelayanan') }}" class="btn btn-primary btn-sm">Saya Mengerti, Lanjutkan Pengajuan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
