<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPelayananRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_pelayanan', function (Blueprint $table) {
            $table->foreign('id_pelayanan')
                ->references('id')
                ->on('pelayanan')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('id_satuan_kerja')
                ->references('id')
                ->on('satuan_kerja')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_pelayanan', function (Blueprint $table) {
            //
        });
    }
}
