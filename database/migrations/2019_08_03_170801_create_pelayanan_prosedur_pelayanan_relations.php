<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelayananProsedurPelayananRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prosedur_pelayanan', function (Blueprint $table) {
            $table->foreign('id_pelayanan')
                ->references('id')
                ->on('pelayanan')
                ->onDelete('RESTRICT')
                ->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prosedur_pelayanan', function (Blueprint $table) {
            //
        });
    }
}
