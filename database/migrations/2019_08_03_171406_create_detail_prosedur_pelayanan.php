<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailProsedurPelayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_prosedur_pelayanan', function (Blueprint $table) {
            $table->foreign('id_detail_pelayanan')
                ->references('id')
                ->on('detail_pelayanan')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('id_prosedur_pelayanan')
                ->references('id')
                ->on('prosedur_pelayanan')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_prosedur_pelayanan', function (Blueprint $table) {
            //
        });
    }
}
