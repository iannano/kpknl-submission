<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pelayanan')->unsigned();
            $table->integer('id_satuan_kerja')->unsigned();
            $table->integer('id_pegawai')->nullable();
            $table->string("tiket", 255)->comment("Kode Tiket Pencarian");
            $table->dateTime('waktu_berakhir')->nullable()->comment("Perkiraan Waktu Selesai");
            $table->dateTime('waktu_mulai')->nullable();
            $table->dateTime('waktu_selesai')->nullable();
            $table->integer('id_detail_pelayanan')->unsigned()->comment("Apabila Pelayanan Berkelanjutan")->nullable();
            $table->tinyInteger('status')
                ->comment('0 = Belum diproses | 1 = Sedang diproses | 2 = Berhenti Sementara | 3 = Selesai');
            $table->timestamps();

            //Table Property
            $table->index(['id', 'id_pelayanan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pelayanan');
    }
}
