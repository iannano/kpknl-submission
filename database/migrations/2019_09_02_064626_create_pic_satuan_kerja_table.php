<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicSatuanKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pic_satuan_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_satuan_kerja')->unsigned();
            $table->integer('id_pegawai')->unsigned();
            $table->integer('id_seksi')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pic_satuan_kerja');
    }
}
