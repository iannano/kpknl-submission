<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProsedurPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prosedur_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pelayanan')->unsigned();
            $table->string('prosedur', 255);
            $table->integer('tahapan');
            $table->timestamps();
            $table->softDeletes();

            //Table Property
            $table->index(['id','id_pelayanan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prosedur_pelayanan');
    }
}
