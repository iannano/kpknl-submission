<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatuanKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuan_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('satuan_kerja', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('email', 255);
            $table->string("telegram",255)->nullable();
            $table->string("chat_id", 255)->nullable();
            $table->string('phone', 255);
            $table->timestamps();
            $table->softDeletes();

            //Table Property
            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuan_kerja');
    }
}
