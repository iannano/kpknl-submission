<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dokumen')->unsigned();
            $table->integer('id_detail_pelayanan')->unsigned();
            $table->string('path', 255);
            $table->timestamps();

            //Table Property
            $table->index(['id','id_detail_pelayanan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_pelayanan');
    }
}
