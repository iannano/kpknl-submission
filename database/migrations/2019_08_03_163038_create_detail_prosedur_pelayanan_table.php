<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailProsedurPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_prosedur_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_detail_pelayanan')->unsigned();
            $table->integer('id_prosedur_pelayanan')->unsigned();
            $table->dateTime('waktu');
            $table->timestamps();

            //Table Property
            $table->index(['id','id_detail_pelayanan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_prosedur_pelayanan');
    }
}
