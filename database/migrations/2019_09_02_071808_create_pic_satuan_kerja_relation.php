<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicSatuanKerjaRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pic_satuan_kerja', function (Blueprint $table) {
            $table->foreign('id_satuan_kerja')
                ->references('id')
                ->on('satuan_kerja')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('id_pegawai')
                ->references('id')
                ->on('pegawai')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('id_seksi')
                ->references('id')
                ->on('seksi')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pic_satuan_kerja', function (Blueprint $table) {
            //
        });
    }
}
