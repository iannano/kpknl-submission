<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_seksi')->unsigned();
            $table->string('nama', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->enum('role',['admin','kepala_kantor','kepala_seksi','pegawai']);
            $table->string('telegram',255)->nullable();
            $table->string('chat_id', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            //Table Property
            $table->index(['id','id_seksi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
