<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_seksi')->unsigned();
            $table->string('pelayanan', 255);
            $table->string('slug', 255);
            $table->integer('waktu_sop')->comment("Dalam hitungan hari kerja");
            $table->integer('biaya')->comment("0 Tidak ada biaya pelayanan, apabila dibawah 100 maka menggunakan %, dan diatas 100 menggunakan Rp");
            $table->enum('jenis',["internal","external"]);
            $table->tinyInteger('status')->comment('0 = Non Aktif | 1 = Aktif');
            $table->text('keterangan');
            $table->timestamps();
            $table->softDeletes();

            //Table Property
            $table->index(['id','id_seksi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelayanan');
    }
}
