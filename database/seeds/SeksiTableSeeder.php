<?php

use Illuminate\Database\Seeder;

class SeksiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seksi')->insert([
            'seksi'     => 'Umum',
            'banner'    => '',
            'slug'      => 'umum',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Pengelolaan Kekayaan Negara',
            'banner'    => '',
            'slug'      => 'pengelolaan-kekayaan-negara',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Pelayanan Penilaian',
            'banner'    => '',
            'slug'      => 'pelayanan-penilaian',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Piutang Negara',
            'banner'    => '',
            'slug'      => 'piutang-negara',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Pelayanan Lelang',
            'banner'    => '',
            'slug'      => 'pelayanan-lelang',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Hukum dan Informasi',
            'banner'    => '',
            'slug'      => 'hukum-dan-informasi',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('seksi')->insert([
            'seksi' => 'Kepatuhan Internal',
            'banner'    => '',
            'slug'      => 'kepatuhan-internal',
            'created_at' => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);
    }
}
