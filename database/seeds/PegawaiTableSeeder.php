<?php

use Illuminate\Database\Seeder;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pegawai')->insert([
            "id_seksi"      => 1,
            "nama"          => "Administrator",
            "username"      => "admin",
            "password"      => sha1(md5("admin")),
            "role"          => "Admin",
            "telegram"      => "ibnuauliana",
            "created_at"    => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);
    }
}
