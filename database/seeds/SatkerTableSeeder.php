<?php

use Illuminate\Database\Seeder;

class SatkerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++)
        {
            DB::table('satuan_kerja')->insert([
                "satuan_kerja"  => "satker".$i,
                "username"      => "satker".$i,
                "password"      => sha1(md5("satker".$i)),
                "email"         => "satker".$i."@gmail.com",
                "phone"         => "12345",
                "created_at"    => \Carbon\Carbon::now()->format("Y-m-d H:i:s")
            ]);
        }
    }
}
