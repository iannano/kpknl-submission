<?php

use Illuminate\Database\Seeder;

class PelayananTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelayanan')->insert([
            "id_seksi"  => 2,
            "pelayanan" => "Penetapan Status Penggunaan BMN Berupa Tanah dan/atau Bangunan",
            "slug"      => "penetapan-status-penggunaan-bmn-berupa-tanah-dan-atau-bangunan",
            "waktu_sop" => 5,
            "biaya"     => 0,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('pelayanan')->insert([
            "id_seksi"  => 2,
            "pelayanan" => "Persetujuan/Penolakan Penjualan BMN Selain Tanah dan/atau Bangunan",
            "slug"      => "persetujuan-penolakan-penjualan-bmn-selain-tanah-dan-atau-bangunan",
            "waktu_sop" => 7,
            "biaya"     => 0,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('pelayanan')->insert([
            "id_seksi"  => 4,
            "pelayanan" => "Pelayanan Permohonan Keringanan Utang Pada KPKNL",
            "slug"      => "pelayanan-permohonan-keringanan-utang-pada-kpknl",
            "waktu_sop" => 15,
            "biaya"     => 10,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('pelayanan')->insert([
            "id_seksi"  => 4,
            "pelayanan" => "Pelayanan Permohonan Penarikan Pengurusan Piutang Negara",
            "slug"      => "pelayanan-permohonan-penarikan-penguruan-piutang-negara",
            "waktu_sop" => 3,
            "biaya"     => 2.5,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('pelayanan')->insert([
            "id_seksi"  => 4,
            "pelayanan" => "Pelayanan Permohonan Penebusan Barang Jaminan Senilai/di Atas Nilai Pengikatan",
            "slug"      => "pelayanan-permohonan-penebusan-barang-jaminan-senilai-diatas-nilai-pengikatan",
            "waktu_sop" => 4,
            "biaya"     => 10,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);

        DB::table('pelayanan')->insert([
            "id_seksi"  => 4,
            "pelayanan" => "Penerbitan Surat Pernyataan Piutang Negara Lunas (SPPNL)",
            "slug"      => "penerbitan-surat-pernyataan-piutang-negara-lunas-sppnl",
            "waktu_sop" => 1,
            "biaya"     => 0,
            "jenis"     => "external",
            "status"    => 1,
            "keterangan"=> "-",
            "created_at"=> \Carbon\Carbon::now()->format("Y-m-d H:i:s")
        ]);
    }
}
